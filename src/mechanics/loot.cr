enum LootQuality
  Surface
  Good
  Average
end

enum LootType
  Statue
  Skull
  Flower
  Compass
  Rope

  def self.generate(quality)
    case quality
    in .surface?
      rand < 0.8 ? LootType::Flower : [LootType::Skull, LootType::Statue].sample
    in .good?
      rand < 0.5 ? [LootType::Flower].sample : [LootType::Skull, LootType::Statue, LootType::Compass].sample
    in .average?
      rand < 0.8 ? [LootType::Flower, LootType::Rope].sample : [LootType::Skull, LootType::Statue].sample
    end
  end
end

LOOT_IMG = [RES::Catrine, RES::Skull, RES::Flower, RES::Compass, RES::Rope]

@[ECS::SingletonComponent]
record Inventory < ECS::Component, statues = 1, skulls = 0, flowers = 0, compass = false, rope = 0 do
  def change(typ, value)
    case typ
    when .statue?
      @statues += value
    when .skull?
      @skulls += value
    when .flower?
      @flowers += value
    when .compass?
      @compass = value >= 0
    when .rope?
      if value > 0
        value *= cfg.rope_piece
      else
        value = -@rope if value < -@rope
      end
      @rope += value
    end
  end

  def reset
    @statues = 1
    @skulls = 0
    @flowers = 0
    @rope = 0
    @compass = false
  end

  def makes_sense_rescue
    true
  end
end

class ShowInventory < ECS::System
  def init
    @world.new_entity.add(Inventory.new)
  end

  def execute
    Engine.layer = 100
    inv = @world.getInventory
    # def draw_text_boxed(text, x, y, w, h, halign : HAlign = HAlign::Left, valign : VAlign = VAlign::Center)
    RES::Catrine.draw(57 + 16, 48 + 16)
    font = Fonts[F::Inventory]
    font.draw_text_boxed(inv.statues.to_s, 0, 50, 60, 30, HAlign::Right)

    RES::Flower.draw(80 + 57 + 5 + 16, 48 + 16)
    font.draw_text_boxed(inv.flowers.to_s, 80, 50, 60, 30, HAlign::Right)

    RES::Skull.draw(57 + 160 + 5 + 16, 48 + 16)
    font.draw_text_boxed(inv.skulls.to_s, 160, 50, 60, 30, HAlign::Right)

    RES::Rope.draw(57 + 16, 48 + 16 + 32 + 12)
    n = inv.rope
    if ent = @world.query(RopeUser).find_entity?
      n -= ent.getRopeUser.usage(ent)
    end
    font.draw_text(n.to_s + "м.", 50 + 32, 108)
  end
end

record WillBeLootable < ECS::Component, typ : LootType, tick : Int32

class MakeSomethingLootable < ECS::System
  def filter(world)
    world.of(WillBeLootable)
  end

  def process(entity)
    item = entity.getWillBeLootable
    if entity.getGameState.tick >= item.tick
      entity.replace(WillBeLootable, Lootable.new(item.typ))
    end
  end
end

record Lootable < ECS::Component, typ : LootType

class RemoveLootOnHitSystem < ECS::System
  def filter(world) : ECS::Filter?
    world.of(PlayerHitLootEvent)
  end

  def process(entity)
    ev = entity.getPlayerHitLootEvent

    if loot = ev.second.getLootable?
      inv = entity.getInventory_ptr
      case loot.typ
      when .statue?  then show_modal(TutorialMessages::FoundStatue)
      when .skull?   then show_modal(TutorialMessages::FoundSkull)
      when .flower?  then show_modal(TutorialMessages::FoundFlower)
      when .compass? then show_modal(TutorialMessages::FoundCompass)
      when .rope?    then show_modal(TutorialMessages::FoundRope)
      end
      inv.value.change(loot.typ, 1)
      ev.second.remove(Lootable)
      ev.second.add(PhysicsBodyRemoveRequest.new(also_entity: true))
    end
  end
end

def create_loot(world, x, y, quality)
  typ = LootType.generate(quality)
  ent = world.new_entity
  ent.add(PartOfLevel.new)
  ent.add(Lootable.new(typ))
  ent.add(RenderSprite.new(image: LOOT_IMG[typ.to_i]))
  ent.add(RenderPosition.new(x: 0, y: 0))
  # ent.add(RenderRotation.new)
  ent.add(PhysicsBodyAddRequest.new(PhysicsMaterial::Loot, x: x, y: y))
  ent.add(PhysicsPositionComponent.new(typ: LibEngine::PhysicCoordinatesMode::Read, x: 0, y: 0, vx: 0, vy: 0, a: 0, omega: 0))
end

def throw_item(world, x, y, tox, toy, speed, material)
  ent = world.new_entity
  ent.add(PartOfLevel.new)

  if material.flower?
    ent.add(RenderPosition.new(x: 0, y: 0))
    ent.add(RenderRotation.new)
    ent.add(RenderSprite.new(image: LOOT_IMG[LootType::Flower.to_i]))
    ent.add(WillBeLootable.new(LootType::Flower, tick: ent.getGameState.tick + 60))
  elsif material.hook?
    ent.add(RopeUser.new)
  end

  # ent.add(RenderScale.new(0.5, 0.5))
  ent.add(PhysicsBodyAddRequest.new(material, x: x.to_f64, y: y.to_f64))
  ent.add(PhysicsPositionComponent.new(typ: LibEngine::PhysicCoordinatesMode::Read, x: 0, y: 0, vx: 0, vy: 0, a: 0, omega: 0))
  vec2 = {tox - x, toy - y}
  length = Math.sqrt(vec2[0]*vec2[0] + vec2[1]*vec2[1])
  ent.add(PhysicsPositionRequest.new(
    typ: LibEngine::PhysicCoordinatesMode::Increment,
    vx: vec2[0] / length * speed,
    vy: vec2[1] / length * speed,
  ))
  return ent
end

record ThrowDelay < ECS::Component, cur_delay : Int32 = 0 do
  def decrement
    @cur_delay -= 1
  end
end

record Thrower, ticks_delay : Int32, material : PhysicsMaterial, speed : Float64

@[ECS::SingleFrame]
record ShouldThrow < ECS::Component, tox : Float64, toy : Float64, weapon : Thrower

class ThrowingSystem < ECS::System
  def filter(world)
    world.all_of([ShouldThrow, RenderPosition]).exclude(ThrowDelay)
  end

  def process(entity)
    pos = entity.getRenderPosition
    cmd = entity.getShouldThrow
    weapon = cmd.weapon
    entity.set(ThrowDelay.new(weapon.ticks_delay))
    throw_item(@world, pos.x, pos.y, cmd.tox, cmd.toy, weapon.speed, weapon.material)
  end
end

class ReloadingSystem < ECS::System
  def filter(world)
    world.of(ThrowDelay)
  end

  def process(entity)
    ptr = entity.getThrowDelay_ptr
    ptr.value.decrement
    entity.remove(ThrowDelay) if ptr.value.cur_delay <= 0
  end
end

class FlowerThrowingSystem < ECS::System
  def execute
    return unless Engine::Mouse.left.clicked?
    inv = @world.getInventory_ptr
    return if inv.value.flowers <= 0
    player = get_the_player
    return if player.has? ThrowDelay
    # spend flowers
    inv.value.change(LootType::Flower, -1)
    # and go shoot
    pos = player.getRenderPosition
    player.add(ShouldThrow.new(
      tox: Float64.new(Engine::Mouse.x - Engine[Params::Width]/2 + pos.x),
      toy: Float64.new(Engine::Mouse.y - Engine[Params::Height]/2 + pos.y),
      weapon: Thrower.new(ticks_delay: cfg.throw_flower_delay, material: PhysicsMaterial::Flower, speed: cfg.throw_flower_speed)
    ))
  end
end

class HitMonsters < ECS::System
  def filter(world)
    world.of(EnemyHitByFlower)
  end

  def process(entity)
    ev = entity.getEnemyHitByFlower
    return if ev.first.has?(EnemyDying) || ev.first.has?(EnemyDead)
    show_modal(TutorialMessages::KilledMonster)
    ev.first.add(EnemyDying.new)
    ev.second.add(PhysicsBodyRemoveRequest.new(also_entity: true))
  end
end

@[ECS::SingletonComponent]
record InterestingPoints < ECS::Component, data : Array(InterestingPoint)

@[ECS::SingleFrame]
record UseSkullEvent < ECS::Component
@[ECS::SingleFrame]
record UseStatueEvent < ECS::Component

class ProcessSkullsUse < ECS::System
  def filter(world)
    world.of(UseSkullEvent)
  end

  def process(entity)
    inv = entity.getInventory_ptr
    return if inv.value.skulls <= 0
    player = get_the_player
    return if player.has? AfterTeleport
    return if player.has? IsTeleporting
    return if entity.getGameLevel.level < 2
    all_points = player.getInterestingPoints.data
    pos = player.getPhysicsPositionComponent
    return if all_points.any? { |pt| ((pt.x + 0.5)*cfg.tile_size - pos.x).abs < cfg.tile_size && ((pt.y + 0.5)*cfg.tile_size - pos.y).abs < cfg.tile_size }
    # spend skull
    show_modal(TutorialMessages::UsedSkull)
    inv.value.change(LootType::Skull, -1)
    # teleport
    target = all_points.sample
    body = player.getPhysicsBodyComponent
    player.add(SavedPhysicsMaterial.new(body.typ))
    player.add(PhysicsBodyRemoveRequest.new(also_entity: false))
    player.add IsTeleporting.new((target.x + 0.5)*cfg.tile_size.to_f64, (target.y + 0.5)*cfg.tile_size.to_f64)
    player.set(RenderColor.new(Engine.color(0, 0, 100, 100)))
  end
end

class ProcessStatueUse < ECS::System
  def filter(world)
    world.of(UseStatueEvent)
  end

  def process(entity)
    inv = @world.getInventory_ptr
    return if inv.value.statues <= 0
    health = get_the_player.getHealth_ptr
    return if health.value.hp >= 90.0
    # spend statue
    show_modal(TutorialMessages::UsedStatue)
    inv.value.change(LootType::Statue, -1)
    # heal
    health.value.hp = 100.0f32
    # entity.add(RestartLevel.new)
  end
end

record CompassTarget < ECS::Component

class ProcessCompass < ECS::System
  def filter(world)
    world.of(CompassTarget)
  end

  def process(entity)
    inv = @world.getInventory
    if inv.compass
      Engine.layer = 100
      player = get_the_player.getPhysicsPositionComponent
      portal = entity.getPhysicsPositionComponent
      angle = Math.atan2(portal.y - player.y, portal.x - player.x)*180.0 / Math::PI
      RES::Compass_back.draw(Engine[Engine::Params::Width] - 80, Engine[Engine::Params::Height] - 80)
      RES::Compass_front.draw(Engine[Engine::Params::Width] - 80, Engine[Engine::Params::Height] - 80, angle: -angle)
    end
  end
end
