record RopeUser < ECS::Component do
  def usage(entity)
    pl = get_the_player
    return 0 unless pl_pos = pl.getPhysicsPositionComponent?
    return 0 unless our_pos = entity.getPhysicsPositionComponent?
    return 0 if our_pos.x == 0 && our_pos.y == 0
    dx, dy = pl_pos.x - our_pos.x, pl_pos.y - our_pos.y
    # p "dx=#{dx}, dy=#{dy}"
    # p "usage #{(Math.sqrt(dx*dx + dy*dy) / cfg.tile_size).to_i}"
    (Math.sqrt(dx*dx + dy*dy) / cfg.tile_size).to_i
  end

  def spend_and_destroy(entity)
    inv = entity.getInventory_ptr
    inv.value.change(LootType::Rope, -usage(entity))
    if entity.has?(PhysicsBodyComponent)
      entity.set(PhysicsBodyRemoveRequest.new(also_entity: true))
    else
      entity.destroy
    end
  end
end

record RopeGrapple < ECS::Component

class RopeThrowingSystem < ECS::System
  def execute
    return unless Engine::Mouse.right.clicked?
    return if @world.component_exists? RopeUser
    inv = @world.getInventory_ptr
    return if inv.value.rope <= 0
    player = get_the_player
    return if player.has? ThrowDelay
    # and go shoot
    pos = player.getRenderPosition
    player.add(ShouldThrow.new(
      tox: Float64.new(Engine::Mouse.x - Engine[Params::Width]/2 + pos.x),
      toy: Float64.new(Engine::Mouse.y - Engine[Params::Height]/2 + pos.y),
      weapon: Thrower.new(ticks_delay: cfg.throw_flower_delay, material: PhysicsMaterial::Hook, speed: cfg.throw_rope_speed)
    ))
  end
end

class CancelRopeSystem < ECS::System
  def execute
    return unless user = @world.query(RopeUser).find_entity?
    if Engine::Mouse.right.clicked? || !get_the_player.has?(PhysicsPositionComponent) || user.getRopeUser.usage(user) > user.getInventory.rope
      user.getRopeUser.spend_and_destroy(user)
    end
  end
end

class ProcessGrappling < ECS::System
  def filter(world)
    world.of(HookCatchWall)
  end

  def process(entity)
    ev = entity.getHookCatchWall
    grapple = @world.new_entity
    grapple.add(ev.first.getPhysicsPositionComponent)
    grapple.add(PartOfLevel.new)
    grapple.add(RopeUser.new)
    grapple.add(RopeGrapple.new)
    ev.first.set(PhysicsBodyRemoveRequest.new(also_entity: true))
  end
end

class RenderRopeSystem < ECS::System
  def filter(world)
    world.of(RopeUser)
  end

  def process(entity)
    pos = entity.getPhysicsPositionComponent
    return if pos.x == 0 && pos.y == 0
    pos2 = get_the_player.getPhysicsPositionComponent?
    return unless pos2
    Engine.layer = 2
    Engine.line_settings(width: 5)
    Engine.line(pos.x, pos.y, pos2.x, pos2.y, Engine::Color::OLIVE)
    Engine.line(pos.x - 1, pos.y, pos2.x - 1, pos2.y, Engine::Color::YELLOW)
    Engine.line(pos.x + 1, pos.y, pos2.x + 1, pos2.y, Engine::Color::YELLOW)
    Engine.line(pos.x, pos.y - 1, pos2.x, pos2.y - 1, Engine::Color::YELLOW)
    Engine.line(pos.x, pos.y + 1, pos2.x, pos2.y + 1, Engine::Color::YELLOW)
    Engine.line_settings(width: 1)
  end
end

class ProcessRopeForce < ECS::System
  def filter(world)
    world.of(RopeGrapple)
  end

  def process(entity)
    pos = entity.getPhysicsPositionComponent
    pl_pos = get_the_player.getPhysicsPositionComponent?
    return unless pl_pos
    dx = pos.x - pl_pos.x
    dy = pos.y - pl_pos.y
    r = Math.sqrt(dx*dx + dy*dy)
    k = cfg.rope_base_force / r
    get_the_player.set(PhysicsApplyForceRequest.new(
      fx: k * dx,
      fy: k * dy,
    ))
  end
end
