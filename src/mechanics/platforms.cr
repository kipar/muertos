record CheckStand < ECS::Component
record IsStanding < ECS::Component
record IsCrawling < ECS::Component
record IsOnRamp < ECS::Component

@[ECS::SingleFrame]
record UnderPressure < ECS::Component

class LandFloorSystem < ECS::System
  def filter(world)
    world.of(PlayerOnFloorEvent)
  end

  def process(entity)
    # set standing if collides with floor
    event = entity.getPlayerOnFloorEvent
    player = event.first
    # return if player.has? IsStanding
    if event.collision.energy > 1e7
      player.set(UnderPressure.new)
    end
    if event.collision.ny > 0.5
      player.set(IsStanding.new)
      if event.collision.nx.abs > 0.5 && event.collision.nx.abs < 0.95
        player.set(IsOnRamp.new)
      else
        player.remove_if_present(IsOnRamp)
      end
      if event.collision.ny < 0.99
        player.set(IsCrawling.new)
      else
        player.remove_if_present(IsCrawling)
      end
    end
  end
end

class FromFloorSystem < ECS::System
  def filter(world)
    world.all_of([CheckStand, IsStanding, PlayerSpeed])
  end

  def process(entity)
    # can't jump if already have vertical velocity
    pos = entity.getPlayerSpeed
    if pos.vy.abs > cfg.gravity / 60 * 2
      entity.remove(IsStanding)
      entity.remove_if_present(IsCrawling)
    end
  end
end

class ProcessMovingFloorSystem < ECS::System
  def filter(world)
    world.of(PlayerOnMovingFloorEvent)
  end

  def process(entity)
    ev = entity.getPlayerOnMovingFloorEvent
    entity.add(PlayerOnFloorEvent.new(ev.first, ev.second, ev.collision))
    mov_pos = ev.second.getPhysicsPositionComponent
    ev.first.set(RelativeSpeed.new(-mov_pos.vx, -mov_pos.vy))
  end
end

@[ECS::SingleFrame]
record RelativeSpeed < ECS::Component, vx : Float64, vy : Float64

record PlayerSpeed < ECS::Component, vx : Float64, vy : Float64

class CalculatePlayerSpeed < ECS::System
  def filter(world)
    world.all_of([CheckStand, PhysicsPositionComponent])
  end

  def process(entity)
    pos = entity.getPhysicsPositionComponent
    vx = pos.vx
    vy = pos.vy
    if rel = entity.getRelativeSpeed?
      vx += rel.vx
      vy += rel.vy
    end
    entity.set(PlayerSpeed.new(vx, vy))
  end
end

record IsMovingPlatform < ECS::Component, horizontal : Bool, limit_min : Float32, limit_max : Float32, vx : Float64, vy : Float64

class MovingPlatformsSystem < ECS::System
  def filter(world) : ECS::Filter?
    world.of(IsMovingPlatform)
  end

  def process(entity)
    platform = entity.getIsMovingPlatform
    pos = entity.getPhysicsPositionComponent
    vx = 0
    vy = 0
    if platform.horizontal
      if pos.x < platform.limit_min
        vx = 1
      elsif pos.x > platform.limit_max
        vx = -1
      end
    else
      if pos.y < platform.limit_min
        vy = 1
      elsif pos.y > platform.limit_max
        vy = -1
      end
    end
    if vx != 0 || vy != 0
      # p pos
      entity.add(PhysicsPositionRequest.new(typ: LibEngine::PhysicCoordinatesMode::Increment, vx: -2*pos.vx, vy: -2*pos.vy))
    end
  end
end
