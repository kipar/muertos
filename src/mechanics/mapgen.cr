require "json"

enum LogicTiles
  Empty      =  0
  Wall       =  1
  MoveUp     =  2
  MoveDown   =  3
  MoveLeft   =  4
  MoveRight  =  5
  Destroys   =  6
  RampDown   =  7
  RampUp     =  8
  Skeleton   =  9
  Ghost      = 10
  PortalFrom = 11
  PortalTo   = 12
  Spikes     = 13
  Bonus      = 14

  ExitNormalEnter = 28
  ExitNormalExit  = 29
  ExitFailEnter   = 30
  ExitFailExit    = 31

  def moving?
    self >= MoveUp && self <= MoveRight
  end

  def ramp?
    self >= RampDown && self <= RampUp
  end

  def monster?
    self >= Skeleton && self <= Ghost
  end

  def looks_like_wall?
    self == Wall || self.moving? || self == Destroys
  end

  def flip_x
    return MoveLeft if self == MoveRight
    return MoveRight if self == MoveLeft
    return RampUp if self == RampDown
    return RampDown if self == RampUp
    self
  end
end

enum RenderTiles
  Floor          = 1
  Ground
  LeftPlatform
  RightPlatform
  RampDown
  RampUp
  UnderRampUp
  UnderRampDown
  PortalTwoSided
  PortalOneSided
  PortalFinal
  Spikes
  Lava
  PropsStart     = 16
end

def flip_prop(x)
  return 5 if x == 6
  return 6 if x == 5
  return x
end

def convert_tile_to_picture(tile, up_neigh, down_neigh, left_neigh, right_neigh, prop = 0)
  render = case tile
           when .looks_like_wall?
             if up_neigh.looks_like_wall?
               RenderTiles::Ground
             elsif tile.moving? && !left_neigh.moving? && !right_neigh.moving?
               RenderTiles::Floor
             elsif tile.moving? && !left_neigh.moving?
               RenderTiles::LeftPlatform
             elsif tile.moving? && !right_neigh.moving?
               RenderTiles::RightPlatform
             elsif up_neigh.ramp_up?
               RenderTiles::UnderRampUp
             elsif up_neigh.ramp_down?
               RenderTiles::UnderRampDown
             elsif up_neigh.empty? && down_neigh.empty? && left_neigh.empty? && right_neigh.empty?
               RenderTiles::Floor
             elsif up_neigh.empty? && down_neigh.empty? && left_neigh.empty?
               RenderTiles::LeftPlatform
             elsif up_neigh.empty? && down_neigh.empty? && right_neigh.empty?
               RenderTiles::RightPlatform
             else
               RenderTiles::Floor
             end
           when .ramp_up?
             RenderTiles::RampUp
           when .ramp_down?
             RenderTiles::RampDown
           when .spikes?
             RenderTiles::Spikes
           when .portal_from?
             RenderTiles::PortalTwoSided
           when .portal_to?
             RenderTiles::PortalTwoSided
           else
             if prop > 0
               RenderTiles.new(RenderTiles::PropsStart.to_i + prop)
             else
               return 0
             end
           end
  render.to_i
end

record MovingPlatform,
  x0 : Int32, y0 : Int32, width : Int32, height : Int32,
  vx : Int32, vy : Int32,
  limit_min : Int32, limit_max : Int32

record Ramp, x0 : Int32, y0 : Int32, dx : Int32
record InterestingPoint, x : Int32, y : Int32
record Spikes, x0 : Int32, y0 : Int32, dx : Int32
record Monster, typ : Int32, x : Int32, y : Int32

enum PortalType
  OneSided
  TwoSided
  Final
end

record Portal, typ : PortalType, from_x : Int32, from_y : Int32, to_x : Int32, to_y : Int32
record Loot, x : Int32, y : Int32, quality : Bool

class GeneratedMap
  getter tile_data = [] of Array(Int32)
  # property horiz_spans = [] of {from_x: Int32, to_x: Int32, }
  getter fill_data = [] of Array(Bool)
  getter movers = [] of MovingPlatform
  getter ramps = [] of Ramp
  getter loot = [] of Loot
  getter monsters = [] of Monster
  getter portals = [] of Portal
  getter special_tile_data = [] of Array(Int32)
  getter spikes = [] of Spikes
  getter interesting_points = [] of InterestingPoint
  property size_x : Int32
  property size_y : Int32
  property start_x : Int32 = 0
  property start_y : Int32 = 0

  def initialize(@size_x, @size_y)
    @tile_data = Array(Array(Int32)).new(@size_x) { Array(Int32).new(@size_y, 0) }
    @fill_data = Array(Array(Bool)).new(@size_x) { Array(Bool).new(@size_y, false) }
    @special_tile_data = Array(Array(Int32)).new(@size_x) { Array(Int32).new(@size_y, 0) }
  end

  def create_portal(typ, from_x, from_y, to_x, to_y)
    case typ
    when .two_sided?
      @tile_data[from_x][from_y] = RenderTiles::PortalTwoSided.to_i
      @tile_data[to_x][to_y] = RenderTiles::PortalTwoSided.to_i
    when .one_sided?
      @tile_data[from_x][from_y] = RenderTiles::PortalOneSided.to_i
    when .final?
      @tile_data[from_x][from_y] = RenderTiles::PortalFinal.to_i
    end
    portals << Portal.new(typ, from_x, from_y, to_x, to_y)
  end
end

abstract class MapGenerator
  abstract def create(map : GeneratedMap)
end

class DummyRandom < MapGenerator
  private def tile_at(map, x, y)
    return LogicTiles::Wall if x < 0
    return LogicTiles::Wall if x >= map.size_x
    return LogicTiles::Wall if y < 0
    return LogicTiles::Wall if y >= map.size_y
    map.fill_data[x][y] ? LogicTiles::Wall : LogicTiles::Empty
  end

  def create(map : GeneratedMap)
    map.size_x.times do |x|
      map.size_y.times do |y|
        if x == 0 || x == map.size_x - 1 || y == 0 # || y == map.size_y - 1
          map.fill_data[x][y] = true
        else
          map.fill_data[x][y] = rand < -0.5
        end
      end
    end
    map.size_x.times do |x|
      map.size_y.times do |y|
        map.tile_data[x][y] = convert_tile_to_picture(
          tile_at(map, x, y),
          tile_at(map, x, y - 1),
          tile_at(map, x, y + 1),
          tile_at(map, x - 1, y),
          tile_at(map, x + 1, y)
        )
        map.tile_data[x][y] = RenderTiles::Lava.to_i if y == map.size_y - 1
      end
    end
    map.spikes << Spikes.new(0, map.size_y - 1, map.size_x)

    loop do
      map.start_x = 1 + rand(map.size_x - 2)
      map.start_y = 1 + rand(map.size_y//2 - 2)
      break if !map.fill_data[map.start_x][map.start_y]
    end
  end
end

class TiledJsonLevel
  @logic : Array(LogicTiles)
  @props : Array(Int32)
  getter size_x : Int32
  getter size_y : Int32
  getter start_x : Int32 = 0
  getter start_y : Int32 = 0
  getter finish_x : Int32 = 0
  getter finish_y : Int32 = 0

  def initialize(@filename : String)
    adata = JSON.parse(File.read(@filename))
    @logic = adata["layers"][0]["data"].as_a.map { |val| val.as_i == 0 ? LogicTiles::Empty : LogicTiles.new(val.as_i - 1) }
    @props = adata["layers"][1]["data"].as_a.map { |val| val.as_i == 0 ? 0 : val.as_i - 32 }
    @size_x = adata["width"].as_i
    @size_y = adata["height"].as_i
    index = @logic.index(LogicTiles::ExitNormalEnter)
    if index
      @start_x = index % @size_x
      @start_y = index // @size_x
    end
    index = @logic.index(LogicTiles::ExitNormalExit)
    if index
      @finish_x = index % @size_x
      @finish_y = index // @size_x
    end
  end

  def initialize(*, flip : TiledJsonLevel)
    @filename = flip.@filename + " (flipped)"
    @size_x = flip.size_x
    @size_y = flip.size_y
    @start_x = flip.size_x - 1 - flip.start_x
    @start_y = flip.start_y
    @logic = Array(LogicTiles).new(flip.@logic.size) do |i|
      x = i % flip.size_x
      y = i // flip.size_x
      fx = flip.size_x - 1 - x
      flip.@logic[fx + y*flip.size_x].flip_x
    end
    @props = Array(Int32).new(flip.@props.size) do |i|
      x = i % flip.size_x
      y = i // flip.size_x
      fx = flip.size_x - 1 - x
      flip_prop(flip.@props[fx + y*flip.size_x])
    end
  end

  def create_moving_platform(map, x0, y0, x, y)
    tile = @logic[x + y*@size_x]
    # direction
    dx = dy = 0
    case tile
    when .move_left?
      dx = -1
    when .move_right?
      dx = 1
    when .move_up?
      dy = -1
    when .move_down?
      dy = 1
    end
    # find sizes
    first_x = x - 1
    while first_x >= 0 && @logic[first_x + y*@size_x] == tile
      first_x -= 1
    end
    first_x += 1
    first_y = y - 1
    while first_y >= 0 && @logic[x + first_y*@size_x] == tile
      first_y -= 1
    end
    first_y += 1
    # move tiles
    (first_x..x).each do |cur_x|
      (first_y..y).each do |cur_y|
        map.special_tile_data[x0 + cur_x][y0 + cur_y] = map.tile_data[x0 + cur_x][y0 + cur_y]
        map.tile_data[x0 + cur_x][y0 + cur_y] = 0
        map.fill_data[x0 + cur_x][y0 + cur_y] = false
      end
    end
    # find limits
    limit_min = limit_max = -1
    if dx != 0
      left = first_x - 1
      check_y = (first_y + y) // 2
      while left > 0 && @logic[left + check_y*@size_x].empty?
        left -= 1
      end
      right = x + 1
      while right < @size_x - 1 && @logic[right + check_y*@size_x].empty?
        right += 1
      end
      limit_min = x0 + left
      limit_max = x0 + right
    elsif dy != 0
      top = first_y - 1
      check_x = (first_x + x) // 2
      while top > 0 && @logic[check_x + top*@size_x].empty?
        top -= 1
      end
      bottom = y + 1
      while bottom < size_y - 1 && @logic[check_x + bottom*@size_x].empty?
        bottom += 1
      end
      limit_min = y0 + top
      limit_max = y0 + bottom
    end
    # add mover
    map.movers << MovingPlatform.new(x0: x0 + first_x, y0: y0 + first_y, width: x - first_x + 1, height: y - first_y + 1, vx: dx, vy: dy, limit_min: limit_min, limit_max: limit_max)
  end

  def create_ramp(map, x0, y0, x, y)
    tile = @logic[x + y*@size_x]
    # direction of x if we move upward
    # down: \    up: /
    dx = tile.ramp_up? ? 1 : -1
    dy = -1
    first_x, first_y = x, y
    loop do
      map.fill_data[x0 + first_x][y0 + first_y] = false
      map.fill_data[x0 + first_x][y0 + first_y + 1] = false
      first_x += dx
      first_y += dy
      break if first_x < 0 || first_y < 0 || first_x > @size_x - 1
      break if @logic[first_x + first_y*@size_x] != tile
      # map.fill_data[x0 - dx + first_x][y0 - dy + first_y + 1] = false # unless first_y - dy == y # to skip first and last element
    end
    first_x -= dx
    first_y -= dy
    map.ramps << Ramp.new(x0: x0 + first_x, y0: y0 + first_y, dx: -dx * ((x - first_x).abs + 1))
  end

  def create_spikes(map, x0, y0, x, y)
    tile = @logic[x + y*@size_x]
    first_x, first_y = x, y
    loop do
      map.fill_data[x0 + first_x][y0 + first_y] = false
      first_x -= 1
      break if first_x < 0 || first_x > @size_x - 1
      break if @logic[first_x + first_y*@size_x] != tile
    end
    first_x += 1
    map.spikes << Spikes.new(x0: x0 + first_x, y0: y0 + first_y, dx: x - first_x + 1)
  end

  def create_monster(map, x0, y0, x, y)
    tile = @logic[x + y*@size_x]
    map.monsters << Monster.new(typ: tile.to_i - LogicTiles::Skeleton.to_i, x: x0 + x, y: y0 + y)
  end

  def create_portal(map, x0, y0, x, y)
    x1, y1 = x + x0, y + y0
    index2 = @logic.index(LogicTiles::PortalTo)
    raise "no matching portal in level #{@filename}" unless index2
    x2, y2 = (index2 % @size_x) + x0, (index2 // @size_x) + y0
    map.create_portal(PortalType::TwoSided, x1, y1, x2, y2)
  end

  def create_loot(map, x0, y0, x, y)
    tile = @logic[x + y*@size_x]
    map.loot << Loot.new(x: x0 + x, y: y0 + y, quality: true)
  end

  private def tile_at(x, y)
    return LogicTiles::Wall if x < 0
    return LogicTiles::Wall if x >= @size_x
    return LogicTiles::Wall if y < 0
    return LogicTiles::Wall if y >= @size_y
    @logic[x + y*@size_x]
  end

  def apply(map, x0, y0)
    @size_y.times do |y|
      @size_x.times do |x|
        # place walls
        tile = @logic[x + y*@size_x]
        unless tile_at(x, y - 1).ramp? && (tile_at(x - 1, y).ramp? || tile_at(x + 1, y).ramp?)
          map.fill_data[x0 + x][y0 + y] = tile.looks_like_wall?
        end
        # add background
        prop = @props[x + y*@size_x]
        map.tile_data[x0 + x][y0 + y] = convert_tile_to_picture(
          tile_at(x, y),
          tile_at(x, y - 1),
          tile_at(x, y + 1),
          tile_at(x - 1, y),
          tile_at(x + 1, y),
          prop
        )
        # map.tile_data[x0 + x][y0 + y] = tile if tile > 0 && rand < 0.2
        # moving objects
        case tile
        when .moving?
          # we found last corner
          if @logic[(x + 1) + y*@size_x] != tile && @logic[x + (y + 1)*@size_x] != tile
            create_moving_platform(map, x0, y0, x, y)
          end
        when .spikes?
          if x == @size_x - 1 || @logic[(x + 1) + y*@size_x] != tile
            create_spikes(map, x0, y0, x, y)
          end
        when .ramp?
          # we found bottom ramp
          if y == @size_y - 1 ||
             (tile.ramp_down? && x == @size_x - 1) ||
             (tile.ramp_down? && @logic[(x + 1) + (y + 1)*@size_x] != tile) ||
             (tile.ramp_up? && x == 0) ||
             (tile.ramp_up? && @logic[(x - 1) + (y + 1)*@size_x] != tile)
            create_ramp(map, x0, y0, x, y)
          end
        when .monster?
          create_monster(map, x0, y0, x, y)
        when .bonus?
          create_loot(map, x0, y0, x, y)
        when .portal_from?
          create_portal(map, x0, y0, x, y)
        end
      end
    end
  end
end

class TiledSingleLevel < MapGenerator
  getter level : TiledJsonLevel
  getter flipped : TiledJsonLevel

  def initialize(filename : String)
    @level = TiledJsonLevel.new(filename)
    @flipped = TiledJsonLevel.new(flip: @level)
  end

  def create(map : GeneratedMap)
    # @flipped.apply(map, 0, 0)
    # @level.apply(map, @level.size_x, 0)
    # @level.apply(map, 0, @level.size_y)
    # @flipped.apply(map, @level.size_x, @level.size_y)
    # map.start_x, map.start_y = @level.size_x - @level.start_x - 1, @level.start_y

    @level.apply(map, 0, 0)
    map.start_x, map.start_y = @level.start_x, @level.start_y
    map.create_portal(PortalType::Final, @level.finish_x, @level.finish_y, 0, 0)

    # map.create_portal(PortalType::TwoSided, @level.size_x // 2, @level.size_y // 2, @level.size_x + @level.size_x // 2, @level.size_y + @level.size_y // 2)
    # map.create_portal(true, map.start_x, map.start_y, @level.size_x + 5, @level.size_x + 5)
  end
end

class TiledGlobalLevel < MapGenerator
  getter levels : Hash(String, Tuple(TiledJsonLevel, TiledJsonLevel))

  def initialize(size : Int32)
    @mapgen = GlobalMapGenerator.new(size)
    @mapgen.init_templates_from_json
    @mapgen.check_templates_list
    @levels = Hash(String, Tuple(TiledJsonLevel, TiledJsonLevel)).new
    @mapgen.templates.each do |templ|
      lvl = TiledJsonLevel.new(templ.name)
      flip = TiledJsonLevel.new(flip: lvl)
      @levels[templ.name] = {lvl, flip}
    end
  end

  def create(map : GeneratedMap)
    loop do
      @mapgen.fill
      break if @mapgen.is_good
    end
    # create bounds
    DummyRandom.new.create(map)
    @mapgen.applications.each do |appl|
      # p "applying #{appl.templ.name} to #{appl.x0}, #{appl.y0}"
      normal, flipped = @levels[appl.templ.name]
      used = appl.flip ? flipped : normal
      used.apply(map, appl.x0, appl.y0)
    end
    map.start_x, map.start_y = @mapgen.chains[0].start_x, @mapgen.chains[0].start_y
    # create_final_portal
    map.create_portal(PortalType::Final, @mapgen.chains[0].end_x, @mapgen.chains[0].end_y, 0, 0)

    other_chain_starts = [] of Tuple(Int32, Int32)
    @mapgen.chains.each do |chain|
      map.interesting_points << InterestingPoint.new(chain.start_x, chain.start_y)
      next if chain == @mapgen.chains[0]
      map.create_portal(PortalType::OneSided, chain.end_x, chain.end_y, map.start_x, map.start_y)
      other_chain_starts << {chain.start_x, chain.start_y}
    end
    other_chain_starts << {map.start_x, map.start_y} if other_chain_starts.empty?
    @mapgen.primary_dead_ends.each do |dead|
      if rand < 0.1
        map.create_portal(PortalType::OneSided, dead.x, dead.y, *other_chain_starts.sample)
      else
        map.loot << Loot.new(dead.x, dead.y, rand < 0.33)
      end
    end
    @mapgen.secondary_dead_ends.each do |dead|
      if rand < 0.33
        map.create_portal(PortalType::OneSided, dead.x, dead.y, map.start_x, map.start_y)
      else
        map.loot << Loot.new(dead.x, dead.y, rand < 0.5)
      end
    end
    map.loot.reject! { |item| item.x == 0 || item.y == 0 || item.x == map.size_x - 1 || item.y == map.size_y - 1 || map.portals.any? { |portal| portal.from_x == item.x && portal.from_y == item.y } }
    map.loot.each_with_index do |item|
      map.tile_data[item.x][item.y] = 0
      map.fill_data[item.x][item.y] = false
    end
  end
end
