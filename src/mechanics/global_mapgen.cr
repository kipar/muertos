require "json"

N_INITIAL_TRIES = 100
N_FINAL_TRIES   = 100
K_CHAIN_TRIES   =  10
ALLOW_FLIPS     = true

enum TemplateExitType
  NormalEnter
  NormalExit
  FailEnter
  FailExit

  def invert
    return TemplateExitType.new((self.to_i // 2)*2 + (self.to_i & 1 > 0 ? 0 : 1))
  end
end

enum ExitDir
  Top
  Bottom
  Left
  Right

  def invert
    return ExitDir.new((self.to_i // 2)*2 + (self.to_i & 1 > 0 ? 0 : 1))
  end

  def horizontal?
    self.to_i >= 2
  end

  def flipped(value = true)
    return self unless value
    horizontal? ? invert : self
  end
end

raise "invert wrong" unless ExitDir::Right.invert == ExitDir::Left
raise "invert wrong" unless ExitDir::Top.invert == ExitDir::Bottom

record TemplateExit, typ : TemplateExitType, pos : Int32

record TemplateExitLocation, typ : TemplateExitType, dir : ExitDir, x : Int32, y : Int32, level : Int32, key : TemplateApplication

class MapTemplate
  property width : Int32
  property height : Int32
  property name = ""
  getter exits = [] of Array(TemplateExit)
  @cached = false
  @cached_initial = false

  def initialize(@width, @height)
    @exits = [[] of TemplateExit, [] of TemplateExit, [] of TemplateExit, [] of TemplateExit]
  end

  def exit_pos(dir, exit, flipped)
    case dir
    when .left?
      {0, exit.pos}
    when .right?
      {width - 1, exit.pos}
    when .top?
      {flipped ? width - exit.pos - 1 : exit.pos, 0}
    when .bottom?
      {flipped ? width - exit.pos - 1 : exit.pos, height - 1}
    else
      raise "bad dir #{dir}"
    end
  end

  def can_be_initial?
    return @cached_initial if @cached
    return false if @width <= 1
    return false if @height <= 1
    result = @exits.any?(&.any?(&.typ.normal_enter?))
    @cached = true
    @cached_initial = result
    result
  end

  def debug_exit_combinations
    possible = Set(Tuple(ExitDir, TemplateExitType)).new
    ExitDir.each do |dir|
      TemplateExitType.each do |typ|
        possible << {dir, typ} if @exits[dir.to_i].any? { |ex| ex.typ == typ }
        if dir.horizontal?
          possible << {dir, typ} if @exits[dir.invert.to_i].any? { |ex| ex.typ == typ }
        end
      end
    end
    possible
  end
end

record TemplateApplication, templ : MapTemplate, x0 : Int32, y0 : Int32, flip : Bool, level : Int32 do
  def intersects?(enemy)
    return false if x0 + templ.width <= enemy.x0
    return false if x0 >= enemy.x0 + enemy.templ.width
    return false if y0 + templ.height <= enemy.y0
    return false if y0 >= enemy.y0 + enemy.templ.height
    true
  end

  def possible?(mapgen)
    return false if @x0 <= 0 || @y0 <= 0 || @x0 + templ.width >= mapgen.size || @y0 + templ.height >= mapgen.size
    !mapgen.applications.any? { |enemy| intersects? enemy }
  end

  def each_exit(typ, offset, &)
    ExitDir.each do |dir|
      fixed_dir = dir.flipped(@flip)
      @templ.exits[fixed_dir.to_i].each do |ex|
        next unless ex.typ == typ
        x, y = @templ.exit_pos(dir, ex, @flip)
        x += x0
        y += y0
        if offset
          case dir
          when .left?
            x -= 1
          when .right?
            x += 1
          when .top?
            y -= 1
          when .bottom?
            y += 1
          end
        end
        yield(TemplateExitLocation.new(typ: typ, dir: dir, x: x, y: y, level: @level, key: self))
      end
    end
  end

  def sample_exit(typ, offset)
    list = [] of TemplateExitLocation
    each_exit(typ, offset) do |loc|
      list << loc
    end
    list.sample
  end
end

record LogicalChain, length : Int32, start_x : Int32, start_y : Int32, end_x : Int32, end_y : Int32 do
  def real_length
    (start_x - end_x).abs + (start_y - end_y).abs
  end
end
record DeadEnd, x : Int32, y : Int32

class GlobalMapGenerator
  getter templates = [] of MapTemplate

  # getter output : Array(Array(Int32))
  getter applications = [] of TemplateApplication
  getter chains = [] of LogicalChain
  getter primary_dead_ends = [] of DeadEnd
  getter secondary_dead_ends = [] of DeadEnd
  getter size : Int32

  def initialize(@size)
    # @output = Array(Array(Int32)).new(@size) { Array(Int32).new(@size, 0) }
  end

  #######################################################################################################

  def n_chain_tries
    @templates.size*K_CHAIN_TRIES
  end

  def initial_random_application
    templ = templates.select(&.can_be_initial?).sample
    offsetx = rand(@size - templ.width)
    offsety = rand(@size - templ.height)
    TemplateApplication.new(templ, offsetx, offsety, false, 0)
  end

  def application_for_exit_type(loc : TemplateExitLocation, except : MapTemplate)
    next_level = loc.level + 1 # loc.typ.normal_enter? ? loc.level + 1 : 0
    list = templates.select { |templ| templ != except && templ.exits[loc.dir.to_i].any? { |ex| ex.typ == loc.typ } }
    count_non_flipped = list.size
    if ALLOW_FLIPS
      list = list.concat(templates.select { |templ| templ != except && templ.exits[loc.dir.flipped.to_i].any? { |ex| ex.typ == loc.typ } })
    end
    # raise "no #{loc.typ} match dir #{loc.dir}!" if list.empty?
    return nil if list.empty?
    index = rand(list.size)
    templ = list[index]
    flipped = index >= count_non_flipped
    exits = templ.exits[loc.dir.flipped(flipped).to_i].select { |ex| ex.typ == loc.typ }
    the_exit = exits.sample
    xpos, ypos = templ.exit_pos(loc.dir, the_exit, flipped)
    # now xpos, ypos should match loc.x, loc.y
    TemplateApplication.new(templ, loc.x - xpos, loc.y - ypos, flipped, level: next_level)
  end

  def try_find_application(n, &)
    appl = nil
    ok = false
    n.times do
      appl = yield
      return nil unless appl
      if appl.possible? self
        ok = true
        break
      end
    end
    return nil unless ok
    appl
  end

  def add_exits_to_list(appl, typ, list)
    # this function does inversion and pos correction
    appl.each_exit(typ, true) do |loc|
      list << {TemplateExitLocation.new(typ: typ.invert, dir: loc.dir.invert, x: loc.x, y: loc.y, level: appl.level, key: appl), appl.templ}
    end
  end

  def try_add_chain(min_chain_size)
    already_has_dead_end = Set(TemplateApplication).new
    list_exits = [] of Tuple(TemplateExitLocation, MapTemplate)
    # first, add random application
    old_count = @applications.size
    old_dead_ends = @primary_dead_ends.size
    first_appl = try_find_application(N_INITIAL_TRIES) { initial_random_application }
    return false unless first_appl
    @applications << first_appl
    count = 0
    # now try to add to main chain
    add_exits_to_list(first_appl, TemplateExitType::NormalExit, list_exits)
    while !list_exits.empty?
      loc, except = list_exits.shift
      appl = try_find_application(n_chain_tries) { application_for_exit_type(loc, except) }
      unless appl
        @primary_dead_ends << DeadEnd.new(loc.x, loc.y) if loc.level > 0 && loc.typ.normal_enter? && !already_has_dead_end.includes?(loc.key)
        already_has_dead_end << loc.key
        next
      end
      @applications << appl
      count += 1 if appl.templ.can_be_initial?
      add_exits_to_list(appl, TemplateExitType::NormalExit, list_exits)
    end
    if count < min_chain_size
      while @applications.size > old_count
        @applications.pop
      end
      while @primary_dead_ends.size > old_dead_ends
        @primary_dead_ends.pop
      end
      return false
    end
    # fill chain information
    # record LogicalChain, length : Int32, start_x : Int32, start_y : Int32, end_x : Int32, end_y : Int32
    max_level = 0
    final = first_appl
    (old_count...@applications.size).each do |i|
      appl = @applications[i]
      if appl.level > max_level
        final = appl
        max_level = appl.level
      end
    end
    if max_level > 0
      start = first_appl.sample_exit(TemplateExitType::NormalEnter, false)
      finish = final.sample_exit(TemplateExitType::NormalExit, false)
      @chains << LogicalChain.new(length: final.level, start_x: start.x, start_y: start.y, end_x: finish.x, end_y: finish.y)
    end

    # now try to fill secondary chains
    (old_count...@applications.size).each do |index|
      add_exits_to_list(@applications[index], TemplateExitType::FailExit, list_exits)
    end
    while !list_exits.empty?
      loc, except = list_exits.shift
      appl = try_find_application(n_chain_tries) { application_for_exit_type(loc, except) }
      unless appl
        if loc.typ.fail_enter?
          appl = try_find_application(n_chain_tries) { application_for_exit_type(TemplateExitLocation.new(TemplateExitType::NormalEnter, loc.dir, loc.x, loc.y, level: 0, key: loc.key), except) }
        end
      end
      unless appl
        @secondary_dead_ends << DeadEnd.new(loc.x, loc.y) if loc.level > 0 && loc.typ.normal_enter? && !already_has_dead_end.includes?(loc.key)
        already_has_dead_end << loc.key
        next
      end
      @applications << appl
      add_exits_to_list(appl, TemplateExitType::NormalExit, list_exits)
      add_exits_to_list(appl, TemplateExitType::FailExit, list_exits)
    end
    return true
  end

  # @once = false

  def fill
    @primary_dead_ends.clear
    @secondary_dead_ends.clear
    @chains.clear
    @applications.clear
    while try_add_chain(5)
      # if @once
      #   debug_draw(false)
      # end
    end
    while try_add_chain(1)
      # if @once
      #   debug_draw(false)
      # end
    end
    while try_add_chain(0)
      # if @once
      #   debug_draw(false)
      # end
    end
    N_FINAL_TRIES.times do
      try_add_chain(0)
      # if @once
      #   debug_draw(false)
      # end
    end
    # @once = true
  end

  def check_json(adata)
    raise "not enough layers" unless adata["layers"].size >= 2
    # @grid_back = adata["layers"][0]["data"].as_a
    # @grid_walls = adata["layers"][1]["data"].as_a
    # @grid_special = adata["layers"][2]["data"].as_a
    raise "wrong layer1 name: #{adata["layers"][0]["name"]}" unless adata["layers"][0]["name"] == "Specials"
    raise "wrong layer2 name: #{adata["layers"][1]["name"]}" unless adata["layers"][1]["name"] == "Background"
    value = "XXX"
    raise "wrong layer0 data: #{value}" if adata["layers"][0]["data"].as_a.any? { |val| value = val; val.as_i != 0 && (val.as_i < 0 || val.as_i > 32) }
    raise "wrong layer1 data: #{value}" if adata["layers"][1]["data"].as_a.any? { |val| value = val; val.as_i != 0 && (val.as_i < 33 || val.as_i > 64) }
  end

  def init_templates_from_json
    Dir.glob("./levels/*.json") do |filename|
      next if filename == "./levels/intro.json"
      adata = JSON.parse(File.read(filename))
      begin
        check_json(adata)
      rescue ex
        puts "#{filename}: #{ex}"
      end
      size_x = adata["width"].as_i
      size_y = adata["height"].as_i
      grid_exits = adata["layers"][0]["data"].as_a.map { |x| x.as_i < 29 ? nil : TemplateExitType.new(x.as_i - 29) }
      template = MapTemplate.new(size_x, size_y)
      template.name = filename
      # now iterate over left\right sides (except corners)
      (1..size_y - 2).each do |y|
        x = 0
        if typ = grid_exits[x + y*size_x]
          template.exits[ExitDir::Left.to_i] << TemplateExit.new(typ, y)
        end
        x = size_x - 1
        if typ = grid_exits[x + y*size_x]
          template.exits[ExitDir::Right.to_i] << TemplateExit.new(typ, y)
        end
      end
      # now iterate over left\right sides (including corners corners)
      (0..size_x - 1).each do |x|
        y = 0
        if typ = grid_exits[x + y*size_x]
          template.exits[ExitDir::Top.to_i] << TemplateExit.new(typ, x) unless typ.normal_exit?
          # special case for corner
          if (x == 0) && (typ.normal_enter? || typ.fail_enter?)
            template.exits[ExitDir::Left.to_i] << TemplateExit.new(typ, y)
          end
          if (x == size_x - 1) && (typ.normal_enter? || typ.fail_enter?)
            template.exits[ExitDir::Right.to_i] << TemplateExit.new(typ, y)
          end
        end
        y = size_y - 1
        if typ = grid_exits[x + y*size_x]
          template.exits[ExitDir::Bottom.to_i] << TemplateExit.new(typ, x)
          # special case for corner
          if (x == 0) && (typ.normal_enter? || typ.fail_enter?)
            template.exits[ExitDir::Left.to_i] << TemplateExit.new(typ, y)
          end
          if (x == size_x - 1) && (typ.normal_enter? || typ.fail_enter?)
            template.exits[ExitDir::Right.to_i] << TemplateExit.new(typ, y)
          end
        end
      end

      @templates << template
    end
  end

  #######################################################################################################

  def check_templates_list
    raise "no startable templates" unless @templates.any? &.can_be_initial?
    possible = Set(Tuple(ExitDir, TemplateExitType)).new
    ExitDir.each do |dir|
      TemplateExitType.each do |typ|
        possible << {dir, typ} if @templates.any?(&.exits[dir.to_i].any? { |ex| ex.typ == typ })
        if dir.horizontal?
          possible << {dir, typ} if @templates.any?(&.exits[dir.invert.to_i].any? { |ex| ex.typ == typ })
        end
      end
    end
    # @templates.each { |x| puts "#{x.name} : #{x.debug_exit_combinations}" }
    entries = Set(ExitDir).new
    ExitDir.each do |dir|
      {TemplateExitType::NormalExit, TemplateExitType::FailExit}.each do |typ1|
        next unless possible.includes?({dir, typ1})
        typ2 = [TemplateExitType::NormalEnter, TemplateExitType::FailEnter]
        if typ1.normal_exit?
          typ2 = [TemplateExitType::NormalEnter]
        end
        raise "no #{typ2} found for #{dir.invert}" unless typ2.any? { |typ3| possible.includes?({dir.invert, typ3}) }
        n = typ2.sum do |typ3|
          @templates.map(&.debug_exit_combinations).count { |set| set.includes?({dir.invert, typ3}) }
        end
        raise "too little #{typ2} found for #{dir.invert}" if n < 2
      end
    end
  end

  def status
    max_chain = @chains.max_by { |ch| ch.length }
    "#{is_good ? "GOOD!" : "BAD!!!"} ends: #{@primary_dead_ends.size},#{@secondary_dead_ends.size}, max_chain: #{max_chain.length}, #{max_chain.real_length}"
  end

  def is_good
    max_chain = @chains.max_by { |ch| ch.length }
    return false if max_chain.length < 10
    return false if max_chain.real_length < 50
    return false if primary_dead_ends.size + secondary_dead_ends.size > 60
    true
  end
end
