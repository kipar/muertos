record LookDirection < ECS::Component, look_left : Bool = false

enum PlayerFrame
  Idle
  Jump
  Walk1
  Walk2
  Walk3
  Walk4
end

record PlayerAnimation < ECS::Component, frame : PlayerFrame = PlayerFrame::Idle, timer : Int32 = 0 do
  def dec_timer
    @timer -= 1
  end
end

class AnimatePlayerSystem < ECS::System
  def filter(world)
    world.all_of([CheckStand, PlayerSpeed, PlayerAnimation])
  end

  def process(entity)
    state = entity.getPlayerAnimation_ptr
    moving_vertical = entity.getPlayerSpeed.vy.abs > 1e-6
    standing = entity.has?(IsStanding)
    crawling = entity.has?(IsCrawling)
    moving_horizontal = entity.getPlayerSpeed.vx.abs > 1e-6
    frame = PlayerFrame::Idle
    timer = 6
    if standing && !moving_horizontal
      if crawling
        if state.value.timer > 0
          state.value.dec_timer
          return
        end
        frame = state.value.frame == PlayerFrame::Jump ? PlayerFrame::Idle : PlayerFrame::Jump
        timer = 18
      else
        frame = PlayerFrame::Idle
      end
    elsif standing
      old_frame = state.value.frame
      case old_frame
      when .walk1?
        if state.value.timer > 0
          state.value.dec_timer
          return
        end
        frame = PlayerFrame::Walk2
      when .walk2?
        if state.value.timer > 0
          state.value.dec_timer
          return
        end
        frame = PlayerFrame::Walk3
      when .walk3?
        if state.value.timer > 0
          state.value.dec_timer
          return
        end
        frame = PlayerFrame::Walk4
      when .walk4?
        if state.value.timer > 0
          state.value.dec_timer
          return
        end
        frame = PlayerFrame::Walk1
      else
        frame = PlayerFrame::Walk1
      end
    else
      frame = PlayerFrame::Jump
    end
    state.value = PlayerAnimation.new(frame, timer)
  end
end

class ApplyPlayerAnimationSystem < ECS::System
  def filter(world)
    world.all_of([PlayerAnimation, RenderTile])
  end

  def process(entity)
    anim_frame = entity.getPlayerAnimation.frame.to_i
    tile = entity.getRenderTile_ptr
    tile.value.set_frame(anim_frame)
  end
end

class RenderTurnedPlayerSystem < ECS::System
  def filter(world)
    world.all_of([LookDirection, RenderScale])
  end

  def process(entity)
    kx = entity.getLookDirection.look_left ? -1.0f32 : 1.0f32
    old_scale = entity.getRenderScale
    entity.set(RenderScale.new(kx: old_scale.kx.abs*kx, ky: old_scale.ky))
  end
end
