record ReceiveMoveEvents < ECS::Component

@[ECS::SingleFrame]
record MovePlayerEvent < ECS::Component, dx : Int32

class TurnPlayerSystem < ECS::System
  def filter(world) : ECS::Filter?
    world.all_of([MovePlayerEvent, LookDirection])
  end

  def process(ent)
    move = ent.getMovePlayerEvent
    dx = move.dx.to_f64
    ent.set(LookDirection.new(dx < 0))
  end
end

class MovePlayerSystem < ECS::System
  def filter(world) : ECS::Filter?
    world.all_of([PhysicsBodyComponent, PlayerSpeed])
  end

  def process(ent)
    move = ent.getMovePlayerEvent?
    speed = ent.getPlayerSpeed
    pos = ent.getPhysicsPositionComponent
    dx = move ? move.dx.to_f64 : 0.0
    if dx == 0.0
      return unless ent.has?(IsStanding)
      return if @world.query(RopeUser).find_entity?
      power = cfg.player_stop_force
      delta_speed = -speed.vx
      scaled_delta_speed = delta_speed.abs
      return if scaled_delta_speed < 1e-6
      # pp! delta_speed
      if scaled_delta_speed < cfg.speedcontrol_zero
        return if ent.has? IsOnRamp
        return if ent.has? IsCrawling
        # p "patching"
        rspeed_x, rspeed_y = pos.vx - speed.vx, pos.vy - speed.vy
        ent.add(PhysicsPositionRequest.new(typ: LibEngine::PhysicCoordinatesMode::Write, vx: rspeed_x, vy: rspeed_y, x: pos.x, y: pos.y))
        return
      elsif scaled_delta_speed < cfg.speedcontrol_scale
        # p "scaling"
        power = power * (scaled_delta_speed / cfg.speedcontrol_scale)
      else
        # p "full"
      end
      force = delta_speed.sign * power
      ent.add(PhysicsApplyForceRequest.new(fx: force, fy: 0.0))
    else
      on_floor = ent.has?(IsStanding)
      return if speed.vx * dx.sign > cfg.player_speed
      power = on_floor ? cfg.player_walk_force : cfg.player_air_walk_force
      force = dx * power
      fy = 0.0
      # if ent.has? IsOnRamp
      #   return if speed.vx * dx.sign > cfg.player_speed * cfg.ramp_slowing
      #   fy = -(force.abs + cfg.gravity)
      #   # force = force * cfg.ramp_slowing
      # end
      ent.add(PhysicsApplyForceRequest.new(fx: force, fy: fy))
    end
  end
end

@[ECS::SingleFrame]
record JumpPlayerEvent < ECS::Component

record JumpPlayerAccumulator < ECS::Component, total_force : Float64

class JumpPlayerSystem < ECS::System
  def filter(world) : ECS::Filter?
    world.all_of([JumpPlayerEvent, PhysicsBodyComponent]).any_of([JumpPlayerAccumulator, IsStanding])
  end

  def process(ent)
    body = ent.getPhysicsBodyComponent
    if acc = ent.getJumpPlayerAccumulator?
      delta = cfg.player_jump_force_acc
      max = cfg.player_max_jump_force
      force = {delta, max - acc.total_force}.min
      ent.add(PhysicsApplyForceRequest.new(fx: 0.0, fy: -1.0*(force + cfg.gravity)))
      if acc.total_force >= max - delta
        ent.remove(JumpPlayerAccumulator)
      else
        ent.set(JumpPlayerAccumulator.new(acc.total_force + delta))
      end
      ent.remove_if_present(IsStanding)
    elsif ent.has?(IsStanding)
      ent.add(PhysicsApplyForceRequest.new(fx: 0.0, fy: -1.0*cfg.player_jump_force))
      ent.set(JumpPlayerAccumulator.new(cfg.player_jump_force))
      ent.remove(IsStanding)
      # ent.remove_if_present(IsOnRamp)
    end
  end
end

class ResetJumpPlayerSystem < ECS::System
  def filter(world) : ECS::Filter?
    world.of(JumpPlayerAccumulator).exclude(JumpPlayerEvent)
  end

  def process(entity)
    entity.remove(JumpPlayerAccumulator)
  end
end
