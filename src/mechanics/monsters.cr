def create_monster(world, typ, x, y)
  ent = world.new_entity
  ent.add(PartOfLevel.new)
  ent.add(RenderTile.new(image: typ == 1 ? RES::Ghost : RES::Skeleton, frame: 0))
  # ent.add(RenderTile.new(image: RES::Skeleton, frame: 0))
  ent.add(RenderPosition.new(x: 0, y: 0))
  ent.add(RenderRotation.new)
  ent.add(RenderColor.new)                          # for making red
  ent.add(RenderScale.new(kx: 1.25f32, ky: 1.5f32)) # for turning
  ent.add(RenderLayer.new(layer: 2))

  mat = typ == 1 ? PhysicsMaterial::Ghost : PhysicsMaterial::Enemy
  ent.add(PhysicsBodyAddRequest.new(mat, x: x, y: y))

  ent.add(PhysicsPositionComponent.new(typ: LibEngine::PhysicCoordinatesMode::Read, x: 0, y: 0, vx: 0, vy: 0, a: 0, omega: 0))
  ent.add(LookDirection.new(true))
  # ent.add(Health.new(10.0))
  # ent.add(DamagedTimer.new)

  case typ
  when 1
    ent.add(GhostAnimation.new)
    ent.add(GhostAI.new)
  else
    ent.add(SkeletonAnimation.new)
    ent.add(SkeletonAI.new)
  end
  ent.add(InitialPosition.new(x: x, y: y))

  # ent.add(Teleportable.new)
end

enum SkeletonFrame
  Walk1    =  0
  WalkLast = 10
  Stop1    = 11
  StopLast = 14
end

record SkeletonAnimation < ECS::Component, frame : Int32 = SkeletonFrame::Walk1.to_i, timer : Int32 = 0 do
  def dec_timer
    @timer -= 1
  end
end

record SkeletonAI < ECS::Component, dx : Int32 = 1, last_waypoint : Int32 = 0
record SkeletonWaypoint < ECS::Component, dx : Int32

record EnemyDying < ECS::Component
record EnemyDead < ECS::Component

class SkeletonAnimationSystem < ECS::System
  def filter(world)
    world.all_of([SkeletonAnimation, RenderTile]).exclude(EnemyDead)
  end

  def process(entity)
    anim = entity.getSkeletonAnimation_ptr
    if anim.value.timer > 0
      anim.value.dec_timer
    else
      if entity.has? EnemyDying
        if anim.value.frame < SkeletonFrame::Stop1.to_i
          i = SkeletonFrame::Stop1.to_i
        elsif anim.value.frame < SkeletonFrame::StopLast.to_i
          i = anim.value.frame + 1
        else
          entity.add(EnemyDead.new)
          entity.add(PhysicsBodyRemoveRequest.new(also_entity: false))
          return
        end
      else
        i = (anim.value.frame + 1) % SkeletonFrame::WalkLast.to_i
      end
      entity.set(SkeletonAnimation.new(frame: i, timer: 6))
      tile = entity.getRenderTile_ptr
      tile.value.set_frame(i)
    end
  end
end

class SkeletonAISystem < ECS::System
  def filter(world)
    world.all_of([SkeletonAI, PhysicsBodyComponent, PhysicsPositionComponent]).exclude([EnemyDying, EnemyDead])
  end

  def process(ent)
    dx = ent.getSkeletonAI.dx
    ent.set(LookDirection.new(dx < 0))
    return if ent.getGameState.tick < 5 # TODO
    need_vx = dx*cfg.enemy_speed
    force = ent.getPhysicsPositionComponent.vx < need_vx ? cfg.enemy_force : -cfg.enemy_force
    if ent.getGameState.tick - ent.getSkeletonAI.last_waypoint > 60 &&
       ent.getPhysicsPositionComponent.vx.abs < 1e-9
      ent.update(SkeletonAI.new(-dx, ent.getGameState.tick))
    end
    # ent.add(PhysicsApplyForceRequest.new(fx: dx * ENEMY_FORCE, fy: 0.0))
    ent.add(PhysicsApplyForceRequest.new(fx: force, fy: 0.0))
  end
end

class SkeletonTurnOnWaypoint < ECS::System
  def filter(world)
    world.of(EnemyOnWaypoint)
  end

  def process(ent)
    ev = ent.getEnemyOnWaypoint
    skel = ev.first
    way = ev.second.getSkeletonWaypoint
    # skel.set(SkeletonAI.new(-skel.getSkeletonAI.dx))
    skel.set(SkeletonAI.new(way.dx, skel.getGameState.tick))
  end
end

N_GHOST_FRAMES = 9

record GhostAnimation < ECS::Component, frame : Int32 = 0, timer : Int32 = 0 do
  def dec_timer
    @timer -= 1
  end
end
record GhostAI < ECS::Component

class GhostAnimationSystem < ECS::System
  def filter(world)
    world.all_of([GhostAnimation, RenderTile]).exclude(EnemyDead)
  end

  def process(entity)
    anim = entity.getGhostAnimation_ptr
    if anim.value.timer > 0
      anim.value.dec_timer
    else
      i = (anim.value.frame + 1) % N_GHOST_FRAMES
      entity.set(GhostAnimation.new(frame: i, timer: 10))
      tile = entity.getRenderTile_ptr
      tile.value.set_frame(i)
    end
  end
end

record InitialPosition < ECS::Component, x : Float64, y : Float64

class GhostAISystem < ECS::System
  def filter(world)
    world.all_of([GhostAI, PhysicsBodyComponent, PhysicsPositionComponent])
  end

  def process(entity)
    if entity.has? EnemyDead
    elsif entity.has? EnemyDying
      entity.add(EnemyDead.new)
      entity.add(PhysicsBodyRemoveRequest.new(also_entity: false))
      entity.getRenderTile_ptr.value.set_frame(N_GHOST_FRAMES)
    else
      target = get_the_player.getPhysicsPositionComponent?
      pos = entity.getPhysicsPositionComponent
      unless init_pos = entity.getInitialPosition?
        init_pos = InitialPosition.new(pos.x, pos.y - 1)
        entity.set(init_pos)
      end
      if target
        dx, dy = target.x - pos.x, target.y - pos.y
        range = Math.sqrt(dx*dx + dy*dy)
      else
        dx, dy = 0.0, 0.0
        range = 100000.0
      end
      if range > cfg.ghost_range
        dx, dy = init_pos.x - pos.x, init_pos.y - pos.y
        # k = 0.0
      else
        entity.remove(InitialPosition)
        entity.set(LookDirection.new(dx < 0))
      end
      k = cfg.ghost_speed / range
      entity.add(PhysicsPositionRequest.new(typ: LibEngine::PhysicCoordinatesMode::Increment, vx: k*dx - 0.5*pos.vx, vy: k*dy - 0.5*pos.vy))
    end
  end
end
