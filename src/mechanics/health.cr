require "../libnonoengine"
require "../resources"

enum DamageCause
  Monster
  Pressure
  Spikes
end

record DeathCause < ECS::Component, cause : DamageCause

record Health < ECS::Component, hp : Float32 do
  def hp=(value)
    @hp = value
  end
end

@[ECS::MultipleComponents]
record Damage < ECS::Component, hp : Float32

record ShowHealth < ECS::Component

class ShowHPBarSystem < ECS::System
  def filter(world)
    world.all_of([ShowHealth, Health])
  end

  HP_BASE_X =  10
  HP_BASE_Y =  10
  HP_BASE_W = 232
  HP_BASE_H =  56
  HP_BAR_X  =   6
  HP_BAR_Y  =   4
  HP_BAR_W  = 178
  HP_BAR_H  =  20

  def process(entity)
    value = entity.getHealth.hp
    max = 100
    Engine.layer = 100
    RES::Hp_back.draw(HP_BASE_X + HP_BASE_W/2, HP_BASE_Y + HP_BASE_H/2)
    RES::Hp_empty.draw(HP_BASE_X + HP_BAR_X + HP_BAR_W/2, HP_BASE_Y + HP_BAR_Y + HP_BAR_H/2)
    Engine.layer = 101
    RES::Hp_full.tex_rect(
      HP_BASE_X + HP_BAR_X, HP_BASE_Y + HP_BAR_Y,
      HP_BAR_W*value / max, HP_BAR_H
    )
    Engine.layer = 1
  end
end

class ApplyPressureDamage < ECS::System
  def filter(world)
    world.all_of([UnderPressure, Health])
  end

  def process(entity)
    entity.add(Damage.new(cfg.pressure_damage.to_f32 / 60))
    entity.set(DeathCause.new(DamageCause::Pressure))
  end
end

class ApplyMonsterDamage < ECS::System
  def filter(world)
    world.all_of([PlayerHitByEnemyEvent])
  end

  def process(entity)
    ev = entity.getPlayerHitByEnemyEvent
    return if ev.second.has? EnemyDying
    ev.first.add(Damage.new(cfg.monster_damage.to_f32))
    ev.first.set(DeathCause.new(DamageCause::Monster))
    show_modal(TutorialMessages::HitByMonster)
    fx = -ev.collision.nx * cfg.monster_hit_force
    fy = -ev.collision.ny * cfg.monster_hit_force
    ev.first.add(PhysicsApplyForceRequest.new(fx: fx, fy: fy))
  end
end

class ApplySpikesDamage < ECS::System
  def filter(world)
    world.all_of([PlayerOnSpikesEvent])
  end

  def process(entity)
    ev = entity.getPlayerOnSpikesEvent
    ev.first.add(Damage.new(cfg.spikes_damage.to_f32))
    ev.first.set(DeathCause.new(DamageCause::Spikes))
    fx = -ev.collision.nx * cfg.spikes_jump
    fy = -ev.collision.ny * cfg.spikes_jump
    ev.first.add(PhysicsApplyForceRequest.new(fx: fx, fy: fy))
  end
end

class ApplyAllDamage < ECS::System
  def filter(world)
    world.all_of([Damage, Health])
  end

  def process(entity)
    entity.set(Health.new(entity.getHealth.hp - entity.getDamage.hp))
  end
end

record DamagedTimer < ECS::Component, tick : Int32 = 0

class ShowDamageSystem < ECS::System
  def filter(world)
    world.all_of([RenderColor, Health, DamagedTimer]).exclude([IsTeleporting])
  end

  def process(entity)
    damaged = entity.has? Damage
    timer = entity.getDamagedTimer
    if damaged
      entity.set(DamagedTimer.new(cfg.damage_show_time))
    else
      entity.set(DamagedTimer.new(timer.tick - 1)) if timer.tick > 0
    end
    if timer.tick > 0
      entity.set(RenderColor.new(Engine::Color.new(cfg.damage_show_color)))
    else
      entity.set(RenderColor.new(Engine::Color::WHITE))
    end
  end
end

class ResetLevelOnDeath < ECS::System
  def filter(world)
    world.all_of([Health, TrackingCamera])
  end

  def process(entity)
    if entity.getHealth.hp < 0
      entity.add(RestartLevel.new)
    end
  end
end
