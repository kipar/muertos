require "../ecs"
require "../libnonoengine"
require "../resources"
require "../game_utils"

@[ECS::SingleFrame]
record InvertDebugRender < ECS::Component

class DrawDebugSystem < ECS::System
  @last_mem = 0u32
  @measure_gc = MeasureGC.new

  def init
    @active = false
  end

  def do_execute
    @active = !@active if @world.component_exists?(InvertDebugRender)
    super
  end

  def execute
    # GC.collect
    Engine.layer = 20
    LibEngine.debug_physics_render
    Engine.layer = 100
    Fonts[F::Debug].draw_text("GC: #{@measure_gc.value // 1000}", 800, 10)
    n = 30
    @world.stats do |name, count|
      Fonts[F::Debug].draw_text("#{name}: #{count}", 800, n)
      n += 12
    end
  end
end

record TrackingCamera < ECS::Component

class TrackCameraSystem < ECS::System
  def filter(world) : ECS::Filter?
    world.of(TrackingCamera).any_of([RenderPosition, RenderRotation])
  end

  def process(entity)
    dx, dy = if pos = entity.getRenderPosition?
               {pos.x - Engine[Engine::Params::RealWidth]/2, pos.y - Engine[Engine::Params::RealHeight]/2}
             else
               {0.0f32, 0.0f32}
             end
    angle = if rot = entity.getRenderRotation?
              rot.angle
            else
              0.0f32
            end
    camera = entity.getCameraPosition
    # entity.update(CameraPosition.new(dx, dy, camera.kx, camera.ky, angle))
    #
    if first_level?(@world)
      entity.update(CameraPosition.new(dx.to_i.to_f32 + 0.5, dy.to_i.to_f32 + 0.5, cfg.camera.to_f32, cfg.camera.to_f32, angle))
    else
      entity.update(CameraPosition.new(dx.to_f32, dy.to_f32, cfg.camera.to_f32, cfg.camera.to_f32, angle))
    end
  end
end

class ReactPlayerSystem < ECS::System
  getter! input_events : ECS::Filter

  def filter(world) : ECS::Filter?
    world.of(ReceiveMoveEvents)
  end

  def init
    @input_events = @world.any_of([MovePlayerEvent, JumpPlayerEvent]).exclude(ReceiveMoveEvents)
  end

  def process(entity)
    return unless inp = self.input_events.find_entity?
    if ev = inp.getMovePlayerEvent?
      entity.set(ev)
    end
    if ev = inp.getJumpPlayerEvent?
      entity.set(ev)
    end
  end
end

class SyncPositionWithPhysicsSystem < ECS::System
  def filter(world) : ECS::Filter?
    world.of(PhysicsPositionComponent).any_of([RenderPosition, RenderRotation])
  end

  def process(entity)
    phys = entity.getPhysicsPositionComponent
    entity.update(RenderPosition.new(x: phys.x.to_f32, y: phys.y.to_f32)) if entity.has? RenderPosition
    entity.update(RenderRotation.new(angle: phys.a.to_f32)) if entity.has? RenderRotation
  end
end

@[ECS::SingletonComponent]
record GameState < ECS::Component, tick : Int32 = 0 do
  def next_tick
    @tick += 1
  end
end

@[ECS::SingletonComponent]
record GameLevel < ECS::Component, level : Int32 = 0 do
  def inc_level
    @level += 1
  end

  def level_1
    @level = 1
  end
end

class ProcessGameState < ECS::System
  def init
    @world.new_entity.set(GameState.new(tick: 0))
    @world.new_entity.set(GameLevel.new(level: 0))
  end

  def execute
    @world.getGameState_ptr.value.next_tick
    Engine.layer = 100
    # Fonts[F::Level].draw_text("Level: #{@world.getGameLevel.level}", 700, 10)
    Engine.layer = 1
  end
end

class ShowBackgroundSystem < ECS::System
  def execute
    case @world.getGameLevel.level
    when 1
      RES::Graveyard.background
    when 2
      RES::Background.background
    when 3
      RES::Background2.background
    when 4
      RES::Background2.background
    else
      RES::Background.background
    end
  end
end
