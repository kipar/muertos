record Teleportable < ECS::Component
record OneSideTeleportable < ECS::Component
record Teleport < ECS::Component, target_x : Float64, target_y : Float64, one_sided : Bool
record FinalPortal < ECS::Component

record IsTeleporting < ECS::Component, target_x : Float64, target_y : Float64
record AfterTeleport < ECS::Component, target_x : Float64, target_y : Float64
record SavedPhysicsMaterial < ECS::Component, material : PhysicsMaterial

class StartTeleportSystem < ECS::System
  def filter(world)
    world.of(JumpToPortal)
  end

  def process(entity)
    ev = entity.getJumpToPortal
    target = ev.first
    return unless target.has? Teleportable
    return if target.has? IsTeleporting
    return if target.has? AfterTeleport
    if ev.second.has? FinalPortal
      return unless target == get_the_player
      if entity.getGameLevel.level > 3 && !player_near_brother
        show_modal TutorialMessages::WaitForBrother
        return
      end
      entity.add(NextLevel.new)
      return
    end
    portal = ev.second.getTeleport
    return if portal.one_sided && !target.has? OneSideTeleportable
    if body = target.getPhysicsBodyComponent?
      target.add(SavedPhysicsMaterial.new(body.typ))
      target.add(PhysicsBodyRemoveRequest.new(also_entity: false))
    end
    # if target == get_the_player
    #   p "teleport from #{target.getPhysicsPositionComponent} to #{portal.target_x}, #{portal.target_y}"
    # end
    target.add IsTeleporting.new(portal.target_x, portal.target_y)
    target.set(RenderColor.new(Engine::Color.new(0))) if target.has? RenderColor
  end
end

class ProcessTeleportationSystem < ECS::System
  def filter(world)
    world.of(IsTeleporting)
  end

  def process(entity)
    target = entity.getIsTeleporting
    pos = entity.getPhysicsPositionComponent
    delta_x = (target.target_x - pos.x).clamp(-cfg.teleport_speed / 60, cfg.teleport_speed / 60)
    delta_y = (target.target_y - pos.y).clamp(-cfg.teleport_speed / 60, cfg.teleport_speed / 60)
    # p "#{pos} => #{target} : #{delta_x}, #{delta_y} "
    entity.update(PhysicsPositionComponent.new(typ: pos.typ, a: pos.a, omega: 0.0, vx: 0.0, vy: 0.0, x: pos.x + delta_x, y: pos.y + delta_y))
    if delta_x.abs < 1e-6 && delta_y.abs < 1e-6
      # teleport complete
      entity.add(AfterTeleport.new(target.target_x, target.target_y))
      entity.remove(IsTeleporting)
      entity.set(RenderColor.new(Engine::Color::WHITE)) if entity.has? RenderColor
      if mat = entity.getSavedPhysicsMaterial?
        entity.add(PhysicsBodyAddRequest.new(mat.material, target.target_x, target.target_y))
        entity.remove(SavedPhysicsMaterial)
      end
    end
  end
end

class RemoveAfterTeleportSystem < ECS::System
  def filter(world)
    world.all_of([AfterTeleport, RenderPosition])
  end

  def process(entity)
    after = entity.getAfterTeleport
    pos = entity.getRenderPosition
    if (after.target_x - pos.x).abs > cfg.tile_size || (after.target_y - pos.y).abs > cfg.tile_size
      entity.remove(AfterTeleport)
    end
  end
end
