{% if !flag?(:simple_map) %}
  require "./global_mapgen"
{% end %}
require "./mapgen"

record PartOfLevel < ECS::Component

def first_level?(world)
  world.getGameLevel.level == 1
end

def post_process_wall(up_neigh, down_neigh, left_neigh, right_neigh)
  if up_neigh
    RenderTiles::Ground
  elsif !up_neigh && !down_neigh && !left_neigh && !right_neigh
    RenderTiles::Floor
  elsif !up_neigh && !down_neigh && !left_neigh
    RenderTiles::LeftPlatform
  elsif !up_neigh && !down_neigh && !right_neigh
    RenderTiles::RightPlatform
  else
    RenderTiles::Floor
  end
end

def add_render_tile(world, x, y, value, themap)
  return if value <= 0
  return if value >= 16 && !first_level?(world) && rand > 0.33
  return if value == RenderTiles::PortalFinal.to_i && world.getGameLevel.level == 3

  if value > 0 && value < RenderTiles::RampDown.to_i
    up = y == 0 || (1..RenderTiles::UnderRampDown.to_i).includes?(themap.tile_data[x][y - 1])
    down = y == themap.size_y - 1 || (1..RenderTiles::UnderRampDown.to_i).includes?(themap.tile_data[x][y + 1])
    left = x == 0 || (1..RenderTiles::UnderRampDown.to_i).includes?(themap.tile_data[x - 1][y])
    right = x == themap.size_x - 1 || (1..RenderTiles::UnderRampDown.to_i).includes?(themap.tile_data[x + 1][y])
    value = post_process_wall(up, down, left, right).to_i
  end

  ent = world.new_entity
  ent.add(PartOfLevel.new)

  img = case world.getGameLevel.level
        when 1
          RES::Surface
        when 2, 5
          RES::Snow
        else
          RES::Ground
        end
  ent.add(RenderTile.new(image: img, frame: value - 1))
  ent.add(RenderPosition.new(x: (x*cfg.tile_size + cfg.tile_size/2).to_f32, y: (y*cfg.tile_size + cfg.tile_size/2).to_f32))
end

def add_horiz_span(world, from_x, to_x, y)
  ent = world.new_entity
  ent.add(PartOfLevel.new)
  ent.add(PhysicsBodyAddRequest.new(PhysicsMaterial::Floor, 0.0, 0.0))
  ent.add(PhysicsAddBoxShapeRequest.new((from_x*cfg.tile_size).to_f, (y*cfg.tile_size).to_f, (to_x*cfg.tile_size + cfg.tile_size).to_f, (y*cfg.tile_size + cfg.tile_size/2).to_f))

  ent = world.new_entity
  ent.add(PartOfLevel.new)
  ent.add(PhysicsBodyAddRequest.new(PhysicsMaterial::EnemyWaypoint, 0.0, 0.0))
  ent.add(SkeletonWaypoint.new(1))
  ent.add(PhysicsAddCircleShapeRequest.new((from_x*cfg.tile_size - 2).to_f, (y*cfg.tile_size - cfg.tile_size/2).to_f, 10))
  ent = world.new_entity
  ent.add(PartOfLevel.new)
  ent.add(PhysicsBodyAddRequest.new(PhysicsMaterial::EnemyWaypoint, 0.0, 0.0))
  ent.add(SkeletonWaypoint.new(-1))
  ent.add(PhysicsAddCircleShapeRequest.new((to_x*cfg.tile_size + cfg.tile_size + 2).to_f, (y*cfg.tile_size - cfg.tile_size/2).to_f, 10))
end

def add_spikes_span(world, from_x, to_x, y)
  ent = world.new_entity
  ent.add(PartOfLevel.new)
  ent.add(PhysicsBodyAddRequest.new(PhysicsMaterial::SpikesFloor, 0.0, 0.0))
  ent.add(PhysicsAddBoxShapeRequest.new((from_x*cfg.tile_size).to_f, (y*cfg.tile_size + cfg.tile_size/2).to_f, (to_x*cfg.tile_size + cfg.tile_size).to_f, (y*cfg.tile_size + cfg.tile_size).to_f))
end

def add_vert_span(world, x, from_y, to_y)
  ent = world.new_entity
  ent.add(PartOfLevel.new)
  ent.add(PhysicsBodyAddRequest.new(PhysicsMaterial::Floor, 0.0, 0.0))
  ent.add(PhysicsAddBoxShapeRequest.new(x*cfg.tile_size.to_f, (from_y + 0.5)*cfg.tile_size.to_f, (x + 1)*cfg.tile_size.to_f, (to_y*cfg.tile_size + cfg.tile_size*9/10).to_f))
end

def add_monster(world, monster)
  return if first_level?(world)
  create_monster(world, monster.typ, (monster.x + 0.5)*cfg.tile_size.to_f, (monster.y + 0.5)*cfg.tile_size.to_f)
end

def add_loot(world, loot, map)
  y = loot.y
  unless first_level?(world)
    while y < map.size_y - 2
      break if map.tile_data[loot.x][y + 1] > 0
      y += 1
    end
    return if y == map.size_y - 2
  end
  create_loot(world, (loot.x + 0.5)*cfg.tile_size.to_f, (y + 0.5)*cfg.tile_size.to_f, first_level?(world) ? (LootQuality::Surface) : (loot.quality ? LootQuality::Good : LootQuality::Average))
end

def add_moving_platform(world, platform, special_tile_data)
  ent = world.new_entity
  ent.add(PartOfLevel.new)
  ent.add(PhysicsBodyAddRequest.new(PhysicsMaterial::MovingFloor,
    x: (platform.x0*cfg.tile_size).to_f,
    y: (platform.y0*cfg.tile_size).to_f))
  ent.add(PhysicsAddBoxShapeRequest.new(0, 0, platform.width*cfg.tile_size.to_f, platform.height*cfg.tile_size.to_f))
  ent.add(PhysicsPositionComponent.new(typ: LibEngine::PhysicCoordinatesMode::Read, x: 0, y: 0, vx: 0, vy: 0, a: 0, omega: 0))
  ent.add(PhysicsPositionRequest.new(
    typ: LibEngine::PhysicCoordinatesMode::Increment,
    vx: cfg.platform_speed_x*platform.vx,
    vy: cfg.platform_speed_y*platform.vy))

  ent.add(RenderPosition.new(0.0f32, 0.0f32))
  horiz = platform.vx != 0
  ent.add(IsMovingPlatform.new(horiz,
    (platform.limit_min + (horiz ? 1 : 2))*cfg.tile_size.to_f32,
    (platform.limit_max - (horiz ? platform.width : platform.height))*cfg.tile_size.to_f32,
    cfg.platform_speed_x*platform.vx,
    cfg.platform_speed_y*platform.vy))
  img = case world.getGameLevel.level
        when 1
          RES::Surface
        when 2, 5
          RES::Snow
        else
          RES::Ground
        end
  platform.width.times do |x|
    platform.height.times do |y|
      ent.add(RenderTile.new(image: img, frame: special_tile_data[platform.x0 + x][platform.y0 + y] - 1, dx: ((x + 0.5)*cfg.tile_size).to_f32, dy: ((y + 0.5)*cfg.tile_size).to_f32))
    end
  end
end

def add_ramp(world, ramp, horiz_span_candidates)
  # remove wrong horiz spans
  horiz_span_candidates.reject! do |(from_x, to_x, y)|
    actual_dx = ramp.dx.sign * (ramp.dx.abs - 1)
    range = ramp.dx < 0 ? (ramp.x0 + actual_dx..ramp.x0) : (ramp.x0..ramp.x0 + actual_dx)

    f = from_x == to_x &&
        (from_x - ramp.x0) * ramp.dx.sign == (y - ramp.y0 - 2) &&
        (ramp.y0..ramp.y0 + ramp.dx.abs).includes?(y)
  end
  nice_candidate = horiz_span_candidates.find do |(from_x, to_x, y)|
    (y == ramp.y0) && (from_x == ramp.x0 + 1 || to_x == ramp.x0 - 1)
  end
  # horiz_span_candidates.delete nice_candidate if nice_candidate
  # p nice_candidate if nice_candidate

  ent = world.new_entity
  ent.add(PartOfLevel.new)
  ent.add(PhysicsBodyAddRequest.new(PhysicsMaterial::Floor, 0, 0))

  #  dx    x0
  # y0       1
  #       /-4
  #     /--/
  # dx 2--3
  actual_dx = ramp.dx.sign * (ramp.dx.abs - 1)
  x0 = ramp.x0*cfg.tile_size
  x0 += cfg.tile_size if ramp.dx < 0
  tx = x0 + actual_dx*cfg.tile_size
  tx += cfg.tile_size*ramp.dx.sign
  y0 = ramp.y0*cfg.tile_size
  pt1 = {x0, y0 + 1}.map(&.to_f64)
  pt2 = {tx, y0 + (ramp.dx.abs - 1)*cfg.tile_size + cfg.tile_size}.map(&.to_f64)
  if ramp.dx.abs == 1
    pt3 = {tx, y0 + cfg.tile_size}.map(&.to_f64)
    pt4 = {x0, y0 + cfg.tile_size}.map(&.to_f64)
  else
    pt3 = {tx - 2*cfg.tile_size*ramp.dx.sign, y0 + (ramp.dx.abs - 1)*cfg.tile_size + cfg.tile_size}.map(&.to_f64)
    pt4 = {x0, y0 + 2*cfg.tile_size}.map(&.to_f64)
  end

  # sadly, nonconvex poly are not supported
  # if nice_candidate
  #   # y0       1 ---------------7
  #   #       /-4 5---------------6
  #   #     /--/
  #   # dx 2--3
  #   from_x_p, to_x_p, y_p = nice_candidate
  #   target_x = (to_x_p < ramp.x0) ? from_x_p : to_x_p
  #   delta_p = target_x - ramp.x0
  #   pt5 = {x0, y0 + cfg.tile_size/2}.map(&.to_f64)
  #   pt6 = {x0 + delta_p*cfg.tile_size, y0 + cfg.tile_size/2}.map(&.to_f64)
  #   pt7 = {x0 + delta_p*cfg.tile_size, y0}.map(&.to_f64)
  #   ent.add(PhysicsAddPolyShapeRequest.new([pt1, pt2, pt3, pt4, pt5, pt6, pt7]))
  # else
  ent.add(PhysicsAddPolyShapeRequest.new([pt1, pt2, pt3, pt4]))
  # end
end

def add_one_sided_portal(world, fromx, fromy, tox, toy, one_sided_flag)
  ent = world.new_entity
  ent.add(PartOfLevel.new)
  ent.add(PhysicsBodyAddRequest.new(PhysicsMaterial::PortalIn,
    x: (fromx*cfg.tile_size).to_f,
    y: (fromy*cfg.tile_size).to_f))
  ent.add(PhysicsAddBoxShapeRequest.new(0, 0, cfg.tile_size.to_f, cfg.tile_size.to_f))
  ent.add(Teleport.new(tox*cfg.tile_size + cfg.tile_size/2, toy*cfg.tile_size + cfg.tile_size/2, one_sided: one_sided_flag))
end

def add_final_portal(world, x, y)
  ent = world.new_entity
  if ent.getGameLevel.level == 3
    create_brother(world, x: (x*cfg.tile_size).to_f, y: (y*cfg.tile_size).to_f).add(CompassTarget.new)
    return
  end
  ent.add(PartOfLevel.new)
  ent.add(PhysicsBodyAddRequest.new(PhysicsMaterial::PortalIn,
    x: (x*cfg.tile_size).to_f,
    y: (y*cfg.tile_size).to_f))
  ent.add(PhysicsAddBoxShapeRequest.new(0, 0, cfg.tile_size.to_f, cfg.tile_size.to_f))
  ent.add(PhysicsPositionComponent.new(typ: LibEngine::PhysicCoordinatesMode::Read, x: 0, y: 0, vx: 0, vy: 0, a: 0, omega: 0))
  ent.add(FinalPortal.new)
  ent.add(CompassTarget.new)
end

def add_portal(world, portal)
  case portal.typ
  when .final?
    add_final_portal(world, portal.from_x, portal.from_y)
  when .one_sided?
    add_one_sided_portal world, portal.from_x, portal.from_y, portal.to_x, portal.to_y, true
  when .two_sided?
    add_one_sided_portal world, portal.from_x, portal.from_y, portal.to_x, portal.to_y, false
    add_one_sided_portal world, portal.to_x, portal.to_y, portal.from_x, portal.from_y, false
  end
end

def goto_next_level(world)
  world.getInventory_ptr.value.change(LootType::Compass, -1)
  if user = world.of(RopeUser).find_entity?
    user.getRopeUser.spend_and_destroy(user)
  end
  world.getGameLevel_ptr.value.inc_level
  case world.getGameLevel.level
  when 1
    raw_new_level(world, starting: true)
    return
  when 2
    show_modal(TutorialMessages::GotoFirstLevel)
    RES::Music::Intro.music
  when 3
    NO_MUSIC.music
    show_modal(TutorialMessages::GotoSecondLevel)
    RES::Music::Level1.music
  when 4
    NO_MUSIC.music
    show_modal(TutorialMessages::GotoThirdLevel)
    RES::Music::Level2.music
  when 5
    NO_MUSIC.music
    show_modal(TutorialMessages::GotoFourthLevel)
    RES::Music::Level2.music
  else
    RES::Music::Over.music
    show_modal(TutorialMessages::Won1)
    show_modal(TutorialMessages::Won2)
    show_modal(TutorialMessages::Won3)
    world.new_entity.add(QuitEvent.new)
  end
  raw_new_level(world)
end

def restart_same_level(world)
  inv = world.getInventory_ptr
  level = world.getGameLevel_ptr
  inv.value.change(LootType::Compass, -1)
  has_statues = inv.value.statues > 0
  if has_statues
    show_modal(TutorialMessages::RescuedByStatue)
    inv.value.change(LootType::Statue, -1)
    raw_new_level(world)
  else
    case get_the_player.getDeathCause.cause
    when .monster?  then show_modal(TutorialMessages::KilledByMonster)
    when .pressure? then show_modal(TutorialMessages::KilledByPressure)
    when .spikes?   then show_modal(TutorialMessages::KilledBySpikes)
    end
    world.new_entity.add(QuitEvent.new)
  end
end

def raw_new_level(world, starting = false)
  world.of(PartOfLevel).each_entity &.destroy
  Engine::Physics.reset

  themap = GeneratedMap.new(100, 100)
  # DummyRandom.new.create(themap)
  {% if flag?(:simple_map) %}
    TiledSingleLevel.new(ARGV.size > 0 ? "./levels/#{ARGV[0]}.json" : "./levels/test1.json").create(themap)
  {% else %}
    if ARGV.size > 0
      TiledSingleLevel.new("./levels/#{ARGV[0]}.json").create(themap)
    elsif starting
      TiledSingleLevel.new("./levels/intro.json").create(themap)
    else
      TiledGlobalLevel.new(100).create(themap)
    end
  {% end %}

  # well, render part
  themap.size_x.times do |x|
    themap.size_y.times do |y|
      add_render_tile(world, x, y, themap.tile_data[x][y], themap)
    end
  end

  horiz_span_candidates = [] of Tuple(Int32, Int32, Int32)
  # search horizontal spans
  (0...themap.size_y).each do |y|
    start = nil
    themap.size_x.times do |x|
      has_line = themap.fill_data[x][y] && ((y == 0) || !themap.fill_data[x][y - 1])
      if has_line
        if !start
          start = x
        end
      else
        if start
          horiz_span_candidates << {start, (x - 1), y}
          start = nil
        end
      end
    end
  end

  # search vertical spans
  started = false
  start_pos = 0
  themap.size_x.times do |x|
    start_pos = 0
    themap.size_y.times do |y|
      # p(themap[x])
      has_line = themap.fill_data[x][y]
      #  ((x > 0) && (!map[x - 1][y])) ||
      #  ((x < max_x - 1) && (!map[x + 1][y])) ||
      #  ((y > 0) && (!map[x][y - 1])) ||
      #  ((y < max_y - 1) && (!map[x][y + 1]))
      if has_line
        if !started
          start_pos = y
          started = true
        elsif y == themap.size_y - 1
          add_vert_span(world, x, start_pos, y)
          started = false
        end
      else
        if started
          add_vert_span(world, x, start_pos, y - 1)
          started = false
        end
      end
    end
  end

  # moving platforms
  themap.movers.each do |platform|
    add_moving_platform(world, platform, themap.special_tile_data)
  end

  themap.ramps.each do |ramp|
    add_ramp(world, ramp, horiz_span_candidates)
  end
  horiz_span_candidates.each { |sp| add_horiz_span(world, *sp) }

  themap.spikes.each do |spike|
    add_spikes_span(world, spike.x0, spike.x0 + spike.dx - 1, spike.y0)
  end

  themap.monsters.each do |monster|
    {% if !flag?(:simple_map) %}
      next if (monster.x - themap.start_x).abs < 10 && (monster.y - themap.start_y).abs < 2
    {% end %}
    add_monster(world, monster) if rand < cfg.skeleton_chance
  end

  themap.portals.each do |portal|
    if (portal.from_x - themap.start_x).abs < 2 && (portal.from_y - themap.start_y).abs < 2
      # regenerate level
      world.getInventory_ptr.value.change(LootType::Statue, 1)
      world.new_entity.add(RestartLevel.new)
      return
    end
    if portal.typ.final? && (portal.from_x - themap.start_x).abs < 20 && (portal.from_y - themap.start_y).abs < 20
      # regenerate level
      world.getInventory_ptr.value.change(LootType::Statue, 1)
      world.new_entity.add(RestartLevel.new)
      return
    end

    add_portal(world, portal)
  end

  themap.loot.each do |loot|
    add_loot(world, loot, themap)
  end

  # let's count difficulty
  unless first_level?(world)
    n_skeletons = world.query(SkeletonAI).count_entities
    n_ghosts = world.query(GhostAI).count_entities
    n_loot = world.query(Lootable).count_entities
    n = (cfg.ghost_chance * (n_loot * 2 - n_skeletons)).to_i - n_ghosts
    n = n // 10 if world.getGameLevel.level == 2
    n = n // 3 if world.getGameLevel.level == 5
    n.times do
      x = 1 + rand(themap.size_x - 2)
      y = 1 + rand(themap.size_y - 2)
      next if (x - themap.start_x).abs + (y - themap.start_y).abs < 10
      add_monster(world, Monster.new(typ: 1, x: x, y: y))
    end
    # puts "skeletons: #{n_skeletons}, ghosts: #{n_ghosts + n}, loot: #{n_loot}"
  end

  create_player(world, themap.start_x, themap.start_y)
  if world.getGameLevel.level > 3
    create_brother(world, x: (themap.start_x*cfg.tile_size).to_f, y: (themap.start_y*cfg.tile_size).to_f)
  end

  world.new_entity.set(GameState.new(tick: 0))
  world.new_entity.set(InterestingPoints.new(themap.interesting_points))
end
