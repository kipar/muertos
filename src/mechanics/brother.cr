record BrotherAI < ECS::Component, wants_loot : Bool, near_player : Bool = false
record MarkedByBrother < ECS::Component

THE_BROTHERS = [] of ECS::Entity

def get_the_brother
  THE_BROTHERS[0]
end

def player_near_brother
  pos1 = get_the_player.getPhysicsPositionComponent?
  return false unless pos1
  pos2 = get_the_brother.getPhysicsPositionComponent
  dx, dy = pos1.x - pos2.x, pos1.y - pos2.y
  range = Math.sqrt(dx*dx + dy*dy)
  range < cfg.brother_range
end

def create_brother(world, x, y)
  THE_BROTHERS.clear
  ent = world.new_entity
  THE_BROTHERS << ent

  ent.add(PartOfLevel.new)
  ent.add(BrotherAI.new(wants_loot: ent.getGameLevel.level > 3))
  ent.add(RenderTile.new(RES::Hero, frame: 5))
  # ent.add(RenderTile.new(image: RES::Skeleton, frame: 0))
  ent.add(RenderPosition.new(x: 0, y: 0))
  ent.add(RenderRotation.new)
  ent.add(RenderColor.new(Engine.color(255, 255, 255, 150)))
  ent.add(RenderScale.new(kx: 1.5f32, ky: 1.5f32)) # for turning
  ent.add(RenderLayer.new(layer: 2))

  ent.add(PhysicsBodyAddRequest.new(PhysicsMaterial::Brother, x: x, y: y))
  ent.add(InitialPosition.new(x: x, y: y))
  ent.add(PhysicsPositionComponent.new(typ: LibEngine::PhysicCoordinatesMode::Read, x: 0, y: 0, vx: 0, vy: 0, a: 0, omega: 0))
  ent.add(LookDirection.new(true))
  ent
end

class ProcessBrotherTouch < ECS::System
  def filter(world)
    world.of(BrotherTouch)
  end

  def process(entity)
    ev = entity.getBrotherTouch
    lvl = entity.getGameLevel.level
    if lvl == 3
      entity.set(NextLevel.new)
    else
      # TODO - giving loot?
    end
  end
end

class BrotherAISystem < ECS::System
  def filter(world)
    world.all_of([BrotherAI, PhysicsBodyComponent, PhysicsPositionComponent])
  end

  def rate_loot(own_pos, player_pos, loot)
    dx, dy = loot.x - player_pos.x, target.y - player_pos.y
    range = Math.sqrt(dx*dx + dy*dy)
    return 100000.0 if range > cfg.brother_range - cfg.tile_size
    dx, dy = loot.x - own_pos.x, target.y - own_pos.y
    range = Math.sqrt(dx*dx + dy*dy)
    range
  end

  def find_loot(own_pos, player_pos)
    list = [] of Tuple(ECS::Entity, Float64)
    @world.query(Lootable).each_entity do |entity|
      loot = entity.getPhysicsPositionComponent
      dx, dy = loot.x - player_pos.x, loot.y - player_pos.y
      range = Math.sqrt(dx*dx + dy*dy)
      if range > cfg.brother_range - cfg.tile_size
        entity.remove_if_present(MarkedByBrother)
        next
      end
      dx, dy = loot.x - own_pos.x, loot.y - own_pos.y
      range = Math.sqrt(dx*dx + dy*dy)
      if range > cfg.brother_range
        entity.remove_if_present(MarkedByBrother)
        next
      end
      list << {entity, range}
    end
    return nil if list.empty?
    list.min_by(&.[1])[0].set(MarkedByBrother.new)
  end

  def process(entity)
    pos = entity.getPhysicsPositionComponent
    ai = entity.getBrotherAI
    unless init_pos = entity.getInitialPosition?
      init_pos = InitialPosition.new(pos.x, pos.y - 1)
      entity.set(init_pos)
    end

    player = get_the_player.getPhysicsPositionComponent?
    if wanted_loot = @world.query(MarkedByBrother).find_entity?
      target = wanted_loot.getPhysicsPositionComponent
    else
      target = player
    end

    if target
      dx, dy = target.x - pos.x, target.y - pos.y
      range = Math.sqrt(dx*dx + dy*dy)
      near_now = (!wanted_loot) && range < cfg.tile_size/2
    else
      near_now = false
      dx, dy = 0.0, 0.0
      range = 100000.0
    end
    if range > cfg.brother_range || (ai.near_player && range < cfg.tile_size*2.5)
      dx, dy = init_pos.x - pos.x, init_pos.y - pos.y
      wanted_loot.remove_if_present(MarkedByBrother) if wanted_loot
      # k = 0.0
    else
      entity.remove(InitialPosition)
      entity.set(LookDirection.new(dx < 0))
    end
    if player
      find_loot(pos, player) if ai.wants_loot && (!wanted_loot) # && near_now
    end
    entity.update(BrotherAI.new(wants_loot: ai.wants_loot, near_player: near_now))
    k = cfg.ghost_speed / range
    entity.add(PhysicsPositionRequest.new(typ: LibEngine::PhysicCoordinatesMode::Increment, vx: k*dx - 0.5*pos.vx, vy: k*dy - 0.9*pos.vy))
  end
end
