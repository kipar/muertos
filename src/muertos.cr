require "./ecs"
require "./basic_systems"
require "./game_systems"
require "./physics_systems"

@[ECS::SingleFrame(check: false)]
struct QuitEvent < ECS::Component
end

@[ECS::SingleFrame(check: false)]
struct NextLevel < ECS::Component
end

@[ECS::SingleFrame(check: false)]
struct RestartLevel < ECS::Component
end

ECS.debug_stats

world = ECS::World.new
systems = ECS::Systems.new(world)
  .add(BasicSystems.new(world))
  .add(PhysicSystems.new(world))
game_systems = GameSystems.new(world)
systems.init
game_systems.init
# test_modals
show_modal(TutorialMessages::Welcome1)
show_modal(TutorialMessages::Welcome2)
goto_next_level(world)
loop do
  systems.execute
  game_systems.execute
  if world.component_exists? NextLevel
    world.query(NextLevel).each_entity &.destroy
    goto_next_level(world)
  end
  if world.component_exists? RestartLevel
    world.query(RestartLevel).each_entity &.destroy
    restart_same_level(world)
  end
  break if world.component_exists? QuitEvent
end
systems.teardown
