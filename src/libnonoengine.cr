@[Link("nonoengine")]
lib LibEngine
  alias Coord = Float32
  alias Color = UInt32
  alias String = UInt8*
  type RawResource = Int32
  type Sprite = Int32
  type Sound = Int32
  type Button = Int32
  type TileMap = Int32
  type Font = Int32
  alias FontInstance = Font
  type Panel = Int32
  type Shader = Int32
  type ShaderUniform = Int32
  type ShaderAttribute = Int32
  alias Texture = Sprite
  type VertexList = Int32
  alias PhysicsCoord = Float64
  type Body = Int32
  type BodyID = Int64
  NO_BODY_ID = -1i64
  type Material = Int32
  type Polygon = Int32

  enum Key
    A; B; C; D; E; F; G; H
    I; J; K; L; M; N; O; P
    Q; R; S; T; U; V; W; X
    Y; Z
    Num0; Num1; Num2; Num3; Num4
    Num5; Num6; Num7; Num8; Num9

    Escape
    LControl; LShift; LAlt; LSystem
    RControl; RShift; RAlt; RSystem
    Menu
    LBracket  # [
    RBracket  # ]
    SemiColon # ;
    Comma     # ,
    Period    # .
    Quote     # '
    Slash     # /
    BackSlash # \
    Tilde     # ~
    Equal     # =
    Dash      # -
    Space
    Return
    Backspace
    Tab
    PageUp
    PageDown
    End
    Home
    Insert
    Delete
    Add      # +
    Subtract # -
    Multiply # *
    Divide   # /
    Left     # Left arrow
    Right    # Right arrow
    Up       # Up arrow
    Down     # Down arrow
    Numpad0; Numpad1; Numpad2; Numpad3; Numpad4
    Numpad5; Numpad6; Numpad7; Numpad8; Numpad9
    F1; F2; F3; F4; F5; F6; F7; F8
    F9; F10; F11; F12; F13; F14; F15
    Pause

    # special keys

    Quit = -1
    Any  = -2
  end

  enum MouseButton
    Left
    Right
    Middle
  end

  enum MouseAxis
    X
    Y
    Scroll
  end

  enum KeyState
    Up
    Down
    Pressed
  end

  enum MouseButtonState
    Up
    Down
    Clicked
  end

  enum VAlign
    None
    Top
    Center
    Bottom
    Flow
  end
  enum HAlign
    None
    Left
    Center
    Right
    Flow
  end

  enum EngineValue
    Fullscreen
    Width
    Height
    VSync
    Antialias
    UseLog
    Autoscale
    Volume
    ClearColor
    PhysicsSpeed
    GravityX
    GravityY
    Damping
    ProgressWidth
    ProgressHeight
    ProgressX
    ProgressY
    RealWidth      = 100
    RealHeight     = 101
    FPS            = 102
    DeltaTime      = 103
  end
  # TEngineConfig = Fullscreen..ClearColor;

  @[Flags]
  enum FontStyle
    Bold
    Italic
    Underlined
  end

  enum ButtonState
    Normal
    Hover
    Pressed
    Clicked
  end

  enum GUICoord
    X
    Y
    Width
    Height
    MouseX
    MouseY
  end

  enum PathfindAlgorithm
    AStarNew
    AStarReuse
    DijkstraNew
    DijkstraReuse
  end
  type PathfindCallback = (Float32, Float32, Float32, Float32, Void* -> Float32)

  enum PixelFormat
    AsByte
    AsFloat
  end

  enum VertexListPrimitive
    Points
    Lines
    Triangles
  end

  enum PhysicCoordinatesMode
    Read
    Write
    ReadWrite
    Increment
  end

  enum BodyType
    Dynamic
    Static
    Kinematic
    NonRotating
  end

  enum CollisionType
    Pass
    Hit
    PassDetect
    HitDetect
    Processable
  end

  # Общие функции движка

  fun init = EngineInit(resources : String)
  fun set = EngineSet(param : EngineValue, value : Int32)
  fun get = EngineGet(param : EngineValue) : Int32
  fun process = EngineProcess
  fun raw_resource = RawResource(resource : RawResource, size : Int32*) : Void*
  fun raw_texture = RawTexture(resource : Sprite) : UInt32 # returns opengl handle
  fun log = EngineLog(s : String)
  fun free = EngineFree

  # Обработка ввода

  fun key_state = KeyState(key : Key) : KeyState
  fun mouse_get = MouseGet(axis : MouseAxis) : Coord
  fun mouse_state = MouseState(key : MouseButton) : MouseButtonState

  # 2д-рендер - спрайты

  fun sprite = Sprite(sprite : Sprite, x : Coord, y : Coord, kx : Float32, ky : Float32, angle : Float32, color : Color)
  fun draw_tiled = DrawTiled(tiled : TileMap, frame : Int32, x : Coord, y : Coord, kx : Float32, ky : Float32, angle : Float32, color : Color)
  fun background = Background(sprite : Sprite, kx : Float32, ky : Float32, dx : Float32, dy : Float32, color : Color)

  # 2д-рендер - примитивы

  fun line = Line(x1 : Coord, y1 : Coord, x2 : Coord, y2 : Coord, color1 : Color, color2 : Color)
  # procedure Line(x1, y1, x2, y2: TCoord; color: TColor); overload;
  fun line_settings = LineSettings(width : Float32, stipple : UInt32, stipple_scale : Float32)
  fun ellipse = Ellipse(x : Coord, y : Coord, rx : Coord, ry : Coord, filled : Bool, color1 : Color, color2 : Color, angle : Float32)
  fun rect = Rect(x0 : Coord, y0 : Coord, w : Coord, h : Coord, filled : Bool, color1 : Color, color2 : Color, color3 : Color, color4 : Color, angle : Float32)
  fun point = Point(x : Coord, y : Coord, color : Color)
  fun triangle = Triangle(x1 : Coord, y1 : Coord, color1 : Color, x2 : Coord, y2 : Coord, color2 : Color, x3 : Coord, y3 : Coord, color3 : Color)

  fun textured_triangle = TexturedTriangle(sprite : Sprite, x1 : Coord, y1 : Coord, tx1 : Coord, ty1 : Coord, x2 : Coord, y2 : Coord, tx2 : Coord, ty2 : Coord, x3 : Coord, y3 : Coord, tx3 : Coord, ty3 : Coord)

  # 2д-рендер - дополнительные функции

  fun layer = SetLayer(z : Int32)
  fun camera = Camera(dx : Coord, dy : Coord, kx : Float32, ky : Float32, angle : Float32)

  # 2д-рендер - вывод текста

  fun font_config = FontConfig(font : FontInstance, char_size : Int32, color : Color, styles : FontStyle, kx : Float32, ky : Float32)
  fun font_create = FontCreate(font : Font, char_size : Int32, color : Color, styles : FontStyle, kx : Float32, ky : Float32) : FontInstance
  fun draw_text = DrawText(font : FontInstance, text : String, x : Coord, y : Coord)
  fun draw_text_boxed = DrawTextBoxed(font : FontInstance, text : String, x : Coord, y : Coord, w : Coord, h : Coord, halign : HAlign, valign : VAlign)

  # ГУИ

  fun panel = Panel(id : Panel, parent : Panel, x : Coord, y : Coord, w : Coord, h : Coord, halign : HAlign, valign : VAlign)
  fun button = Button(btn : Button, parent : Panel, x : Coord, y : Coord, w : Coord, h : Coord, halign : HAlign, valign : VAlign, text : String, font : FontInstance, data : Void*) : ButtonState
  fun gui_coord = GetGUICoord(coord : GUICoord) : Coord

  # Звук

  fun play = Play(sound : Sound, volume : Float32, data : Void*)
  fun music = Music(sound : Sound, volume : Float32)
  fun sound_playing = SoundPlaying(sound : Sound, data : Void*) : Bool

  # Поиск пути
  fun pathfind = Pathfind(size_x : Int32, size_y : Int32, algorithm : PathfindAlgorithm, diagonal_cost : Float32,
                          fromx : Int32, fromy : Int32, tox : Int32, toy : Int32,
                          x : Int32*, y : Int32*,
                          callback : PathfindCallback, opaque : Void*)

  # физика
  fun physics_reset = PhysicsReset
  fun body_create = BodyCreate(material : Material, data : BodyID) : Body
  fun body_free = BodyFree(body : Body)
  fun body_add_shape_circle = BodyAddShapeCircle(body : Body, dx : PhysicsCoord, dy : PhysicsCoord, r : PhysicsCoord)
  fun body_add_shape_box = BodyAddShapeBox(body : Body, x1 : PhysicsCoord, y1 : PhysicsCoord, x2 : PhysicsCoord, y2 : PhysicsCoord)
  fun body_add_shape_line = BodyAddShapeLine(body : Body, x1 : PhysicsCoord, y1 : PhysicsCoord, x2 : PhysicsCoord, y2 : PhysicsCoord)
  fun body_apply_force = BodyApplyForce(body : Body, fx : PhysicsCoord, fy : PhysicsCoord, dx : PhysicsCoord, dy : PhysicsCoord, torque : PhysicsCoord)
  fun body_apply_control = BodyApplyControl(body : Body, tx : PhysicsCoord, ty : PhysicsCoord, max_speed : PhysicsCoord, max_force : PhysicsCoord)
  fun body_coords = BodyCoords(body : Body, mode : PhysicCoordinatesMode, x : PhysicsCoord*, y : PhysicsCoord*, vx : PhysicsCoord*, vy : PhysicsCoord*, a : PhysicsCoord*, omega : PhysicsCoord*)
  fun material = Material(material : Material, density : Float64, friction : Float64, elasticity : Float64, special_type : BodyType, def_radius : Float64)
  fun material_collisions = MaterialCollisions(first : Material, second : Material, collision_type : CollisionType)
  fun get_collisions = GetCollisions(body : Body, with_mat : Material, is_first : Bool*, x : PhysicsCoord*, y : PhysicsCoord*, nx : PhysicsCoord*, ny : PhysicsCoord*, energy : PhysicsCoord*, impulsex : PhysicsCoord*, impulsey : PhysicsCoord*) : BodyID*
  fun get_material_collisions = GetMaterialCollisions(mat : Material, with_mat : Material, body1 : BodyID*, body2 : BodyID*, is_first : Bool*, x : PhysicsCoord*, y : PhysicsCoord*, nx : PhysicsCoord*, ny : PhysicsCoord*, energy : PhysicsCoord*, impulsex : PhysicsCoord*, impulsey : PhysicsCoord*) : Bool
  fun set_current_collision_result = SetCurrentCollisionResult(should_hit : Bool)

  fun debug_physics_render = DebugPhysicsRender

  # физикаЖ полигоны
  fun polygon_create = PolygonCreate(capacity : Int32) : Polygon
  fun polygon_free = PolygonFree(poly : Polygon)
  fun polygon_add_point = PolygonAddPoint(poly : Polygon, x : PhysicsCoord, y : PhysicsCoord)
  fun polygon_draw = PolygonDraw(poly : Polygon, x : Coord, y : Coord, angle : Coord, c : Color, sprite : Texture, dx : Coord, dy : Coord, kx : Coord, ky : Coord)
  fun body_add_shape_poly = BodyAddShapePoly(body : Body, poly : Polygon)

  # шейдеры
  fun shader_activate = ShaderActivate(shader : Shader)
  fun shader_handle = ShaderHandle(shader : Shader) : UInt32
  fun uniform_set_int = UniformSetInt(shader : Shader, uniform : ShaderUniform, value : Int32)
  fun uniform_set_float = UniformSetFloat(shader : Shader, uniform : ShaderUniform, value : Float32)
  fun uniform_set_texture = UniformSetTexture(shader : Shader, uniform : ShaderUniform, value : Texture)
  fun uniform_set_ptr = UniformSetPtr(shader : Shader, uniform : ShaderUniform, value : Void*)

  # Пользовательские текстуры
  fun render_to = RenderTo(sprite : Texture)
  fun get_pixel = GetPixel(x : Coord, y : Coord, sprite : Texture) : Color
  fun texture_create = TextureCreate(width : Coord, height : Coord) : Texture
  fun texture_clone = TextureClone(texture : Texture) : Texture
  fun texture_delete = TextureDelete(texture : Texture)
  fun texture_save = TextureSave(texture : Texture, filename : String)
  fun capture_screen = CaptureScreen(x : Coord, y : Coord, width : Coord, height : Coord, dest : Texture)
  fun texture_get_pixels = TextureGetPixels(texture : Texture, width : Coord*, height : Coord*, format : PixelFormat) : Void*
  fun texture_load_pixels = TextureLoadPixels(texture : Texture, value : Void*, format : PixelFormat)

  # буферы вершин
  fun vertex_list_create = VertexListCreate(buffer : Void*, typ : VertexListPrimitive, vertex_size : Int32, n_vertices : Int32) : VertexList
  fun vertex_list_add_field = VertexListAddField(list : VertexList, field : ShaderAttribute)
  fun vertex_list_add_padding = VertexListAddPadding(list : VertexList, n_bytes : Int32)
  fun vertex_list_draw = VertexListDraw(list : VertexList, size : Int32, was_updated : Bool)
  fun vertex_list_copy = VertexListCopy(list : VertexList) : VertexList
  fun vertex_list_delete = VertexListDelete(list : VertexList)
  fun vertex_list_change = VertexListChange(list : VertexList, buffer : Void*, typ : VertexListPrimitive, n_vertices : Int32)
end

module Engine
  extend self
  enum Params
    Fullscreen
    Width
    Height
    VSync
    Antialias
    UseLog
    Autoscale
    Volume
    ClearColor
    PhysicsSpeed
    GravityX
    GravityY
    Damping
    ProgressWidth
    ProgressHeight
    ProgressX
    ProgressY
    RealWidth      = 100
    RealHeight     = 101
    FPS            = 102
    DeltaTime      = 103
  end

  def self.[]=(param : Params, value)
    LibEngine.set(LibEngine::EngineValue.new(param.to_i), value)
  end

  def self.[](param : Params)
    LibEngine.get(LibEngine::EngineValue.new(param.to_i))
  end

  record PanelMetrics, pos : {Float32, Float32}, size : {Float32, Float32}, cursor : {Float32, Float32} do
    def self.read_from_engine
      PanelMetrics.new(
        pos: {LibEngine.gui_coord(LibEngine::GUICoord::X), LibEngine.gui_coord(LibEngine::GUICoord::Y)},
        size: {LibEngine.gui_coord(LibEngine::GUICoord::Width), LibEngine.gui_coord(LibEngine::GUICoord::Height)},
        cursor: {LibEngine.gui_coord(LibEngine::GUICoord::MouseX), LibEngine.gui_coord(LibEngine::GUICoord::MouseY)}
      )
    end

    def center
      {pos[0] + size[0]/2, pos[1] + size[1]/2}
    end
  end

  class RawResource
    @data : LibEngine::RawResource

    def initialize(adata)
      @data = adata.unsafe_as(LibEngine::RawResource)
    end

    def to_unsafe
      @data
    end

    def as_bytes
      ptr = LibEngine.raw_resource(@data, out size)
      Bytes.new(ptr.as(Pointer(UInt8)), size)
    end

    def as_string
      String.new(as_bytes)
    end
  end

  class GUI
    @@panel_counter = 0
    @@panel_parent = 0
    @@button_counter = 0
    # TODO - screen size
    @@metrics = PanelMetrics.new({0f32, 0f32}, {0f32, 0f32}, {0f32, 0f32})

    def self.reset
      @@panel_counter = 0
      @@panel_parent = 0
      @@button_counter = 0
      @@metrics = PanelMetrics.new({0f32, 0f32}, {0f32, 0f32}, {0f32, 0f32})
    end

    def self.parent
      @@panel_parent.unsafe_as(LibEngine::Panel)
    end

    def self.metrics
      @@metrics
    end

    def self.metrics=(x)
      @@metrics = x
    end

    def self.read_metrics
      @@metrics = PanelMetrics.read_from_engine
    end

    def self.parent=(x)
      @@panel_parent = x.to_i
    end

    def self.new_panel
      @@panel_counter += 1
      @@panel_counter.unsafe_as(LibEngine::Panel)
    end

    def self.new_button
      @@button_counter += 1
      @@button_counter.unsafe_as(Pointer(Void))
    end

    def self.pos
      @@metrics.pos
    end

    def self.size
      @@metrics.size
    end

    def self.center
      @@metrics.center
    end

    def self.cursor
      @@metrics.cursor
    end

    def self.button_state
      xg, yg = GUI.cursor
      inside = (0..1).includes?(xg) && (0..1).includes?(yg)
      state = ButtonState::Normal
      if inside
        if Mouse.left.clicked?
          state = ButtonState::Clicked
        elsif Mouse.left.down?
          state = ButtonState::Pressed
        else
          state = ButtonState::Hover
        end
      end
      state
    end
  end

  def process
    LibEngine.process
    GUI.reset
  end

  def log(s)
    LibEngine.log(s.to_unsafe)
  end

  class Sprite
    @data : LibEngine::Sprite

    def initialize(adata)
      @data = adata.unsafe_as(LibEngine::Sprite)
    end

    def to_unsafe
      @data
    end

    def draw(x, y, kx = 1.0, ky = 1.0, angle = 0.0, color = Color::WHITE)
      LibEngine.sprite(to_unsafe, x, y, kx, ky, angle, color)
    end

    def background(kx = 1.0, ky = 1.0, dx = 0.0, dy = 0.0, color = Color::WHITE)
      LibEngine.background(to_unsafe, kx, ky, dx, dy, color)
    end

    def tex_triangle(x1, y1, tx1, ty1, x2, y2, tx2, ty2, x3, y3, tx3, ty3)
      LibEngine.textured_triangle(to_unsafe, x1, y1, tx1, ty1, x2, y2, tx2, ty2, x3, y3, tx3, ty3)
    end

    def tex_triangle(x1, y1, x2, y2, x3, y3, dx = 0, dy = 0, kx = 1, ky = 1)
      LibEngine.textured_triangle(to_unsafe,
        x1, y1, dx, dy,
        x2, y2, dx + (x2 - x1) * kx, dy + (y2 - y1) * ky,
        x3, y3, dx + (x3 - x1) * kx, dy + (y3 - y1) * ky)
    end

    def tex_rect(x1, y1, w, h, dx = 0, dy = 0, kx = 1, ky = 1)
      tex_triangle(
        x1, y1,
        x1, y1 + h,
        x1 + w, y1 + h, dx, dy, kx, ky)
      tex_triangle(
        x1, y1,
        x1 + w, y1,
        x1 + w, y1 + h,
        dx, dy, kx, ky)
    end

    def get_pixel(x, y)
      LibEngine.get_pixel(x, y, to_unsafe)
    end

    def clone
      Texture.new(raw: LibEngine.texture_clone(to_unsafe))
    end
  end

  # struct Texture < Sprite
  # end

  struct TileMap
    @data : Int32

    def initialize(@data)
    end

    def to_unsafe
      @data.unsafe_as(LibEngine::TileMap)
    end

    def draw_frame(frame, x, y, kx = 1.0, ky = 1.0, angle = 0.0, color = Color::WHITE)
      LibEngine.draw_tiled(to_unsafe, frame, x, y, kx, ky, angle, color)
    end
  end

  class Sound
    @data : Int32
    @volume = 100.0f32

    def initialize(@data)
    end

    def to_unsafe
      @data.unsafe_as(LibEngine::Sound)
    end

    def clone
      Sound.new(@data)
    end

    def play(data = self.as(Void*), volume = @volume)
      LibEngine.play(to_unsafe, volume, data)
    end

    def music(volume = @volume)
      LibEngine.music(to_unsafe, volume)
    end

    def playing?(data = self.as(Void*))
      LibEngine.sound_playing(to_unsafe, data)
    end
  end

  enum VAlign
    None
    Top
    Center
    Bottom
    Flow
  end
  enum HAlign
    None
    Left
    Center
    Right
    Flow
  end

  struct FontResource
    @data : Int32

    def initialize(@data)
    end

    def to_unsafe
      @data.unsafe_as(LibEngine::Font)
    end

    def draw_text(text, x, y)
      LibEngine.draw_text(to_unsafe, text, x, y)
    end

    def draw_text_boxed(text, x, y, w, h, halign : HAlign = HAlign::Left, valign : VAlign = VAlign::Center)
      LibEngine.draw_text_boxed(to_unsafe, text, x, y, w, h, LibEngine::HAlign.new(halign.to_i), LibEngine::VAlign.new(valign.to_i))
    end
  end

  struct Font
    BOLD       = 1
    ITALIC     = 2
    UNDERLINED = 4

    @data : LibEngine::FontInstance

    def to_unsafe
      @data.unsafe_as(LibEngine::FontInstance)
    end

    @char_size = 24
    @color = Engine::Color::WHITE
    @styles = 0
    @kx = 1.0f32
    @ky = 1.0f32

    def initialize(res : FontResource, *, @char_size = 24, @color = Engine::Color::WHITE, @styles = 0, @kx = 1, @ky = 1)
      @data = LibEngine.font_create(res.to_unsafe, @char_size, @color, LibEngine::FontStyle.new(@styles), @kx, @ky)
    end

    def initialize(res : FontResource, *, dont_create : Bool)
      if dont_create
        @data = res.to_unsafe
      else
        @data = LibEngine.font_create(res.to_unsafe, @char_size, @color, LibEngine::FontStyle.new(@styles), @kx, @ky)
      end
    end

    private def update_config
      LibEngine.font_config(@data, @char_size, @color, LibEngine::FontStyle.new(@styles), @kx, @ky)
    end

    def char_size=(value)
      @char_size = value
      update_config
    end

    def color=(value)
      @color = value
      update_config
    end

    def styles=(value)
      @styles = value
      update_config
    end

    def kx=(value)
      @kx = value
      update_config
    end

    def ky=(value)
      @ky = value
      update_config
    end

    def draw_text(text, x, y)
      LibEngine.draw_text(to_unsafe, text, x, y)
    end

    def draw_text_boxed(text, x, y, w, h, halign : HAlign = HAlign::Left, valign : VAlign = VAlign::Center)
      LibEngine.draw_text_boxed(to_unsafe, text, x, y, w, h, LibEngine::HAlign.new(halign.to_i), LibEngine::VAlign.new(valign.to_i))
    end
  end

  LAYER_GUI = 100

  def self.layer=(value)
    LibEngine.layer(value)
  end

  def camera(dx = 0, dy = 0, kx = 1, ky = 1, angle = 0)
    LibEngine.camera(dx, dy, kx, ky, angle)
  end

  enum Color : UInt32
    BLACK      = 0x000000FF
    MAROON     = 0x800000FF
    GREEN      = 0x008000FF
    OLIVE      = 0x808000FF
    NAVY       = 0x000080FF
    PURPLE     = 0x800080FF
    TEAL       = 0x008080FF
    GRAY       = 0x808080FF
    SILVER     = 0xC0C0C0FF
    RED        = 0xFF0000FF
    LIME       = 0x00FF00FF
    YELLOW     = 0xFFFF00FF
    BLUE       = 0x0000FFFF
    FUCHSIA    = 0xFF00FFFF
    AQUA       = 0x00FFFFFF
    WHITE      = 0xFFFFFFFF
    MONEYGREEN = 0xC0DCC0FF
    SKYBLUE    = 0xA6CAF0FF
    CREAM      = 0xFFFBF0FF
    MEDGRAY    = 0xA0A0A4FF
  end

  def color(r : Int32, g : Int32, b : Int32, a : Int32 = 255) : Engine::Color
    Color.new((UInt32.new(r) << 24) + (UInt32.new(g) << 16) + (UInt32.new(b) << 8) + (UInt32.new(a) << 0))
  end

  def color(u : UInt32) : Engine::Color
    Color.new(u)
  end

  def line(x1, y1, x2, y2, color1, color2 = color1)
    LibEngine.line(x1, y1, x2, y2, color1, color2)
  end

  def line_settings(width = 1, stipple = 0xFFFFFFFF, stipple_scale = 1)
    LibEngine.line_settings(width, stipple, stipple_scale)
  end

  def ellipse(x, y, rx, ry, filled, color1, color2 = color1, angle = 0)
    LibEngine.ellipse(x, y, rx, ry, filled, color1, color2, angle)
  end

  def circle(x, y, r, filled, color1, color2 = color1)
    LibEngine.ellipse(x, y, r, r, filled, color1, color2, 0)
  end

  def rect(x0, y0, w, h, filled, color1, color2, color3, color4, angle = 0)
    LibEngine.rect x0, y0, w, h, filled, color1, color2, color3, color4, angle
  end

  def rect(x0, y0, w, h, filled, color, angle = 0)
    LibEngine.rect x0, y0, w, h, filled, color, color, color, color, angle
  end

  def rect_gauge(x0, y0, w, h, value, color1, color2, angle = 0)
    LibEngine.rect x0, y0, w, h, true, color2, color2, color2, color2, angle
    LibEngine.rect x0, y0, value*w, h, true, color1, color1, color1, color1, angle
  end

  def point(x, y, color)
    LibEngine.point(x, y, color)
  end

  def triangle(x1, y1, color1, x2, y2, color2, x3, y3, color3)
    LibEngine.triangle x1, y1, color1, x2, y2, color2, x3, y3, color3
  end

  def triangle(x1, y1, x2, y2, x3, y3, color)
    LibEngine.triangle x1, y1, color, x2, y2, color, x3, y3, color
  end

  def panel(x = 0, y = 0, width = 0, height = 0, halign = HAlign::None, valign = VAlign::None, fill : Color? = nil, border : Color? = nil, &)
    own_id = GUI.new_panel
    parent = GUI.parent
    metrics = GUI.metrics
    LibEngine.panel(own_id, parent, x, y, width, height, LibEngine::HAlign.new(halign.to_i), LibEngine::VAlign.new(valign.to_i))
    GUI.read_metrics

    rect(*GUI.pos, *GUI.size, true, fill) if fill
    rect(*GUI.pos, *GUI.size, false, border) if border
    GUI.parent = own_id
    x = yield
    GUI.parent = parent
    GUI.metrics = metrics
    x
  end

  def panel(*args, **args2)
    panel(*args, **args2) do
    end
  end

  def label(txt : String, font : Font, x = 0, y = 0, width = 0, height = 0, halign = HAlign::None, valign = VAlign::None, fill : Color? = nil, border : Color? = nil, text_halign = HAlign::None, text_valign = VAlign::None)
    panel(x, y, width, height, halign, valign, fill, border) do
      font.draw_text_boxed(txt, *GUI.pos, *GUI.size, halign: text_halign, valign: text_valign)
    end
  end

  enum ButtonState
    Normal
    Hover
    Pressed
    Clicked
  end

  DEFAULT_BTN_FONT = Font.new(FontResource.new(0), dont_create: true)

  def button(resource : ButtonResource, x = 0, y = 0, width = 0, height = 0, text : String? = nil, halign : HAlign = HAlign::None, valign : VAlign = VAlign::None, font : (Font | FontResource | Nil) = nil)
    # own_id = GUI.new_button
    LibEngine.button(resource.to_unsafe, GUI.parent, x, y, width, height, LibEngine::HAlign.new(halign.to_i), LibEngine::VAlign.new(valign.to_i), (text ? text.to_unsafe : nil), font || DEFAULT_BTN_FONT, nil).unsafe_as(ButtonState)
  end

  def button(*args, **args2, &)
    metrics = GUI.metrics
    state = button(*args, **args2)
    GUI.read_metrics
    x = yield(state) unless state.normal?
    GUI.metrics = metrics
    x
  end

  def button_clicked(*args, **args2, &)
    metrics = GUI.metrics
    state = button(*args, **args2)
    GUI.read_metrics
    x = yield if state.clicked?
    GUI.metrics = metrics
    x
  end

  struct ButtonResource
    @data : Int32

    def initialize(@data)
    end

    def to_unsafe
      @data.unsafe_as(LibEngine::Button)
    end
  end

  alias Key = LibEngine::Key

  module Keys
    def self.[](x)
      LibEngine.key_state(x)
    end
  end

  enum MouseButtonState
    Up
    Down
    Clicked
  end

  module Mouse
    def self.x
      LibEngine.mouse_get(LibEngine::MouseAxis::X)
    end

    def self.y
      LibEngine.mouse_get(LibEngine::MouseAxis::Y)
    end

    def self.scroll
      LibEngine.mouse_get(LibEngine::MouseAxis::Scroll)
    end

    def self.left
      MouseButtonState.new(LibEngine.mouse_state(LibEngine::MouseButton::Left).to_i)
    end

    def self.right
      MouseButtonState.new(LibEngine.mouse_state(LibEngine::MouseButton::Right).to_i)
    end

    def self.middle
      MouseButtonState.new(LibEngine.mouse_state(LibEngine::MouseButton::Middle).to_i)
    end
  end

  DELETED_TEXTURE = (-1).unsafe_as(LibEngine::Sprite)

  class Texture < Sprite
    @width : Int32?
    @height : Int32?
    @pixels_as_byte : Slice(UInt32)?
    @pixels_as_float : Slice(Float32)?
    @is_floating : Bool

    def render_to
      LibEngine.render_to(to_unsafe)
    end

    def initialize(@width, @height, *, @is_floating = false)
      @data = LibEngine.texture_create(width, height)
    end

    def from_gpu
      if @is_floating
        ptr = LibEngine.texture_get_pixels(@data, out w, out h, LibEngine::PixelFormat::AsFloat).as(Float32*)
        @pixels_as_float = ptr.to_slice(w.to_i*h.to_i*4)
      else
        ptr = LibEngine.texture_get_pixels(@data, out w, out h, LibEngine::PixelFormat::AsByte).as(UInt32*)
        @pixels_as_byte = ptr.to_slice(w.to_i*h.to_i)
      end
      @width = w.to_i
      @height = h.to_i
    end

    def to_gpu
      if @is_floating
        return unless ptr = @pixels_as_float
        LibEngine.texture_load_pixels(to_unsafe, ptr, LibEngine::PixelFormat::AsFloat)
      else
        return unless ptr = @pixels_as_byte
        LibEngine.texture_load_pixels(to_unsafe, ptr, LibEngine::PixelFormat::AsByte)
      end
    end

    def initialize(*, raw : LibEngine::Sprite, @is_floating = false)
      @data = raw
    end

    def finalize
      free
    end

    def free
      return if @data.unsafe_as(Int32) < 0
      LibEngine.texture_delete(to_unsafe)
      @data = DELETED_TEXTURE
    end

    def save(filename)
      LibEngine.texture_save(to_unsafe, filename)
    end

    def capture_screen(x, y, width, height)
      LibEngine.capture_screen(x, y, width, height, to_unsafe)
    end

    def self.from_screen(x = 0, y = 0, width = Engine[Params::Width], height = Engine[Params::Height])
      new(width, height).tap do |tex|
        tex.capture_screen(x, y, width, height)
      end
    end

    # TODO - api to get size without loading pixels?
    def width
      unless w = @width
        from_gpu
        w = @width.not_nil!
      end
      w
    end

    def height
      unless h = @height
        from_gpu
        h = @height.not_nil!
      end
      h
    end

    def pixels_as_float : Slice(Float32)
      unless ptr = @pixels_as_float
        @is_floating = true
        from_gpu
        ptr = @pixels_as_float.not_nil!
      end
      ptr
    end

    def pixels_as_int : Slice(UInt32)
      unless ptr = @pixels_as_byte
        @is_floating = false
        from_gpu
        ptr = @pixels_as_byte.not_nil!
      end
      ptr
    end
  end

  alias Vec2 = StaticArray(Float32, 2)
  alias Vec3 = StaticArray(Float32, 3)
  alias Vec4 = StaticArray(Float32, 4)

  def vec2(x : Float32, y : Float32)
    Vec2[x, y]
  end

  def vec3(x : Float32, y : Float32, z : Float32)
    Vec3[x, y, z]
  end

  def vec4(x : Float32, y : Float32, z : Float32, w : Float32)
    Vec4[x, y, z, w]
  end

  private def vec2_default
    vec2(0, 0)
  end

  private def vec3_default
    vec3(0, 0, 0)
  end

  private def vec4_default
    vec4(0, 0, 0, 0)
  end

  alias Sampler2d = LibEngine::Texture

  private def sampler2D_default
    Sprite.new(0)
  end

  private def int_default
    0
  end

  private def float_default
    0.0f32
  end

  TYPES_MAPPING = {
    "vec2"      => "Vec2",
    "vec3"      => "Vec3",
    "vec4"      => "Vec4",
    "float"     => "Float32",
    "sampler2D" => "Sprite",
    "int"       => "Int32",
  }

  class UniformsArray(T, N)
    property! owner : Shader
    @id : LibEngine::ShaderUniform
    @data : StaticArray(T, N)

    def initialize(aid, value)
      @id = aid.unsafe_as(LibEngine::ShaderUniform)
      @data = StaticArray(T, N).new(value)
    end

    def []=(index, value)
      unsafe_set(index, value)
      apply
    end

    def [](index)
      @data[index]
    end

    def unsafe_set(index, value)
      @data[index] = T.new(value)
    end

    def apply
      LibEngine.uniform_set_ptr(owner.to_unsafe, @id, pointerof(@data))
    end
  end

  class Shader
    macro uniform(name, typ, id)
      @value_{{name.id}} : {{TYPES_MAPPING[typ.stringify].id}} = {{typ.id}}_default
      def {{name.id}}=(value : {{TYPES_MAPPING[typ.stringify].id}})
        @value_{{name.id}} = value
        {% if typ.id == :sampler2D %}
          LibEngine.uniform_set_texture(to_unsafe, {{id}}.unsafe_as(LibEngine::ShaderUniform), @value_{{name.id}})
        {% else %}
          LibEngine.uniform_set_ptr(to_unsafe, {{id}}.unsafe_as(LibEngine::ShaderUniform), pointerof(@value_{{name.id}}))
        {% end %}
      end

      def {{name.id}} : {{typ.stringify.capitalize.id}}
        @value_{{name.id}}
      end
    end

    macro uniform_array(name, count, typ, id)
      @value_{{name.id}} = UniformsArray({{TYPES_MAPPING[typ.stringify].id}}, {{count}}).new({{id}}, {{typ.id}}_default)

      def {{name.id}} 
        @value_{{name.id}}.owner = self
        @value_{{name.id}}
      end

    end

    macro attribute(name, typ, id)
    end

    def initialize(@data : Int32)
    end

    def to_unsafe
      @data.unsafe_as(LibEngine::Shader)
    end

    def activate
      LibEngine.shader_activate(self)
    end
  end

  class DefaultShader < Shader
    uniform screen_size, vec2, 0
    uniform tex, sampler2D, 1
    attribute color, vec4, 0
    attribute pos, vec3, 1
    attribute texpos, vec3, 2
  end

  #   fun vertex_list_add_field = VertexListAddField(list : VertexList, field : ShaderAttribute)
  #   fun vertex_list_add_padding = VertexListAddPadding(list : VertexList, n_bytes : Int32)
  #   fun vertex_list_copy = VertexListCopy(list : VertexList) : VertexList
  #   fun vertex_list_change = VertexListChange(list : VertexList, buffer : Void*, typ : VertexListPrimitive, n_vertices : Int32)
  DELETED_VERTEX_LIST = (-1).unsafe_as(LibEngine::VertexList)

  # TODO - vertex array
  enum VertexListPrimitive
    Points
    Lines
    Triangles
  end

  class VertexList
    @data : LibEngine::VertexList

    macro vertex(args)
      record Vertex, field : Int32
      VERTEX_SIZE = sizeof(Vertex)
      getter items = [] of Vertex
    end

    @typ : VertexListPrimitive

    def initialize(@typ, start_count = 128)
      @items.capacity = start_count
      @data = LibEngine.vertex_list_create(@items.to_unsafe, @typ, VERTEX_SIZE, start_count)
    end

    # TODO vertex list clone
    # def initialize(*, from_raw : LibEngine::VertexList)
    # end

    # def clone
    #   self.class.new(from_raw: LibEngine.vertex_list_copy(@data))
    # end

    def to_unsafe
      @data
    end

    def draw(*, limit_size : Int32? = nil, was_updated : Bool = False)
      LibEngine.vertex_list_draw(@data, limit_size || -1, was_updated)
    end

    def finalize
      free
    end

    def free
      return if @data.unsafe_as(Int32) < 0
      LibEngine.vertex_list_delete(to_unsafe)
      @data = DELETED_VERTEX_LIST
    end
  end

  module Physics
    def self.reset
      LibEngine.physics_reset
    end
  end

  private MATERIAL_HASH = {} of Body.class => LibEngine::Material

  record MaterialData, density : Float64, friction : Float64, elasticity : Float64, def_radius : Float64
  private MATERIAL_DATA = {} of Body.class => MaterialData
  private MATERIAL_COLL = {} of Tuple(Body.class, Body.class) => LibEngine::CollisionType

  annotation Collides
  end

  # TODO - optimize not reading what is not needed

  struct CollisionData(X)
    getter data : X
    getter first : Bool
    getter x : Float64
    getter y : Float64
    getter nx : Float64
    getter ny : Float64
    getter energy : Float64
    getter impulsex : Float64
    getter impulsey : Float64

    def first?
      @first
    end

    def initialize(@data, @first, @x, @y, @nx, @ny, @energy, @impulsex, @impulsey)
    end

    def self.try_new(body, with_mat)
      ptr = LibEngine.get_collisions(body, with_mat, out afirst, out ax, out ay, out nx, out ny, out aenergy, out aimpulsex, out aimpulsey)
      if ptr == NO_BODY_ID
        return nil
      else
        return self.new(ptr.unsafe_as(X), afirst, ax, ay, nx, ny, aenergy, aimpulsex, aimpulsey)
      end
    end
  end

  class Polygon
    @raw : LibEngine::Polygon?

    def to_unsafe
      @raw.not_nil!
    end

    def initialize(points : Array(Tuple(Float64, Float64))? = nil)
      @raw = LibEngine.polygon_create(points ? points.size : 0)
      if points
        points.each do |(x, y)|
          LibEngine.polygon_add_point(to_unsafe, x, y)
        end
      end
    end

    def add_point(x, y)
      LibEngine.polygon_add_point(to_unsafe, x, y)
    end

    def draw(x, y, a, color = Color::WHITE)
      LibEngine.polygon_draw(to_unsafe, x, y, a, color, -1.unsafe_as(LibEngine::Sprite), 0, 0, 1, 1)
    end

    def draw_textured(x, y, a, tex : Texture, color = Color::WHITE, dx = 0, dy = 0, kx = 1, ky = 1)
      LibEngine.polygon_draw(to_unsafe, x, y, a, color, tex.to_unsafe, dx, dy, kx, ky)
    end

    def free
      if r = @raw
        LibEngine.polygon_free r
        @raw = nil
      end
    end

    def finalize
      free
    end
  end

  class Body
    @raw : LibEngine::Body?
    property x = 0f64
    property y = 0f64
    property vx = 0f64
    property vy = 0f64
    property angle = 0f64
    property omega = 0f64

    def to_unsafe
      @raw.not_nil!
    end

    def update_coords(mode : LibEngine::PhysicCoordinatesMode = LibEngine::PhysicCoordinatesMode::ReadWrite)
      LibEngine.body_coords(to_unsafe, LibEngine::PhysicCoordinatesMode::ReadWrite, pointerof(@x), pointerof(@y), pointerof(@vx), pointerof(@vy), pointerof(@angle), pointerof(@omega))
    end

    def warp_to(*, x = nil, y = nil, vx = nil, vy = nil, angle = nil, omega = nil)
      @x = Float64.new(x) if x
      @y = Float64.new(y) if y
      @vx = Float64.new(vx) if vx
      @vy = Float64.new(vy) if vy
      @angle = Float64.new(angle) if angle
      @omega = Float64.new(omega) if omega
    end

    def initialize(@x = 0f64, @y = 0f64, @vx = 0f64, @vy = 0f64, @angle = 0f64, @omega = 0f64)
      @raw = LibEngine.body_create(MATERIAL_HASH[self.class], owner.unsafe_as(Pointer(Void)).unsafe_as(BodyID))
    end

    def owner
      self
    end

    def self.body_type
      LibEngine::BodyType::Dynamic
    end

    macro collide(klass)
      MATERIAL_COLL[{self, {{klass}}}] = LibEngine::CollisionType::Hit
    end

    macro detect_collide(klass, &)
      MATERIAL_COLL[{self, {{klass}}}] = LibEngine::CollisionType::HitDetect

      @[Collides(with: {{klass}}, processable: false)]
      def collision_{{@type}}_{{klass}}(coll)
        {{yield}} 
      end
    end

    macro can_collide(klass, &)
      MATERIAL_COLL[{self,  {{klass}}}] = LibEngine::CollisionType::Processable

      @[Collides(with: {{klass}}, processable: true)]
      def collision_{{@type}}_{{klass}}(coll)
        {{yield}} 
      end
    end

    macro pass(klass)
      MATERIAL_COLL[{self,  {{klass}}}] = LibEngine::CollisionType::Pass
    end

    macro detect_pass(klass, &)
      MATERIAL_COLL[{self,  {{klass}}}] = LibEngine::CollisionType::PassDetect
      @[Collides(with: {{klass}}, processable: false)]
      def collision_{{@type}}_{{klass}}(coll)
        {{yield}} 
      end
    end

    macro inherited
      {% verbatim do %}
        macro finished
          def process_all_collisions
            {% for x in @type.methods %}
              {% if x.annotation(Collides) %}
                {% ann = x.annotation(Collides) %}
                  loop do
                    %coll = CollisionData({{ann[:with]}}).try_new(to_unsafe, MATERIAL_HASH[{{ann[:with]}}])
                    if %coll
                      result = {{x.name}}(%coll)
                      {% if ann[:processable] %}
                        LibEngine.set_current_collision_result(result)
                      {% end %}
                    else
                      break
                    end

                  end  
              {% end %}  
            {% end %}
          end
        end
      {% end %}    
    end

    macro material(density, friction, elasticity, default_radius = 0)
      MATERIAL_DATA[self] = MaterialData.new({{density}}, {{friction}}, {{elasticity}}, {{default_radius}})
    end

    macro inherited
      # puts "registering #{self} as #{(MATERIAL_HASH.keys.size+1)}"
      MATERIAL_HASH[self] = (MATERIAL_HASH.keys.size+1).unsafe_as(LibEngine::Material)
    end

    def add_box(x1, y1, x2, y2)
      LibEngine.body_add_shape_box(to_unsafe, x1, y1, x2, y2)
    end

    def add_line(x1, y1, x2, y2)
      LibEngine.body_add_shape_line(to_unsafe, x1, y1, x2, y2)
    end

    def add_circle(dx, dy, r)
      LibEngine.body_add_shape_circle(to_unsafe, dx, dy, r)
    end

    def add_poly(poly : Polygon)
      LibEngine.body_add_shape_poly(to_unsafe, poly.to_unsafe)
    end

    def apply_force(fx, fy, dx, dy, torque)
      LibEngine.body_apply_force(to_unsafe, fx, fy, dx, dy, torque)
    end

    def apply_control(tx, ty, max_speed, max_force)
      LibEngine.body_apply_control(to_unsafe, tx, ty, max_speed, max_force)
    end

    def free
      if r = @raw
        LibEngine.body_free r
        @raw = nil
      end
    end

    def finalize
      free
    end

    def process
      update_coords
      process_all_collisions
    end
  end

  class StaticBody < Body
    def self.body_type
      LibEngine::BodyType::Static
    end
  end

  class KinematicBody < Body
    def self.body_type
      LibEngine::BodyType::Kinematic
    end
  end

  #   fun get_collisions = GetCollisions(body : Body, with_mat : Material, is_first : Bool*, x : PhysicsCoord*, y : PhysicsCoord*, nx : PhysicsCoord*, ny : PhysicsCoord*, energy : PhysicsCoord*, impulsex : PhysicsCoord*, impulsey : PhysicsCoord*) : Void*
  #   fun get_material_collisions = GetMaterialCollisions(mat : Material, with_mat : Material, body1 : Void*, body2 : Void*, is_first : Bool*, x : PhysicsCoord*, y : PhysicsCoord*, nx : PhysicsCoord*, ny : PhysicsCoord*, energy : PhysicsCoord*, impulsex : PhysicsCoord*, impulsey : PhysicsCoord*) : Bool
  #   fun set_current_collision_result = SetCurrentCollisionResult(should_hit : Bool)

  def self.init(dir)
    LibEngine.init dir
    MATERIAL_HASH.each do |k, v|
      next if k == StaticBody
      next if k == KinematicBody
      data = MATERIAL_DATA[k]
      typ = k.body_type
      LibEngine.material(v, data.density, data.friction, data.elasticity, typ, data.def_radius)
    end
    MATERIAL_COLL.each do |k, v|
      LibEngine.material_collisions(MATERIAL_HASH[k[0]], MATERIAL_HASH[k[1]], v)
    end
  end
end
