require "./ecs"
require "./basic_systems"
require "./mechanics/global_mapgen"

@[ECS::SingleFrame(check: false)]
struct QuitEvent < ECS::Component
end

class GameConfig
  def self.init_config
  end
end

class GlobalMapGenerator
  #######################################################################################################

  DRAW_OFFSET =  2
  DRAW_TILE   = 10
  DRAW_COLORS =
    {Engine::Color::BLACK,
     Engine::Color::MAROON,
     Engine::Color::GREEN,
     Engine::Color::OLIVE,
     Engine::Color::NAVY,
     Engine::Color::PURPLE,
     Engine::Color::TEAL,
     Engine::Color::GRAY,
     Engine::Color::SILVER,
     Engine::Color::RED,
     Engine::Color::LIME,
     Engine::Color::YELLOW,
     Engine::Color::BLUE,
     Engine::Color::FUCHSIA,
     Engine::Color::AQUA,
     Engine::Color::MONEYGREEN,
     Engine::Color::SKYBLUE,
     Engine::Color::CREAM,
     Engine::Color::MEDGRAY}

  EXIT_COLORS = {
    Engine::Color::YELLOW,
    Engine::Color::LIME,
    Engine::Color::AQUA,
    Engine::Color::RED,
  }

  def debug_draw_exit(x, y, typ)
    # Engine.rect(x, y, DRAW_TILE, DRAW_TILE, true, Engine::Color::WHITE)
    Engine.circle(x + DRAW_TILE/2, y + DRAW_TILE/2, DRAW_TILE/2, true, Engine::Color::WHITE)
    Engine.circle(x + DRAW_TILE/2, y + DRAW_TILE/2, DRAW_TILE/3, true, EXIT_COLORS[typ.to_i])
  end

  @show_incomplete = false

  @font : Engine::Font?

  def init_font
    afont = Font.new(RES::Font)
    afont.char_size = 26
    afont.color = Engine::Color::BLACK
    @font = afont
    afont
  end

  def debug_draw(finished)
    # TODO - still no luck because Engine.process isn't called
    @show_incomplete = !@show_incomplete if Engine::Keys[Engine::Key::P].pressed?
    return unless finished || @show_incomplete
    Engine.rect(DRAW_OFFSET, DRAW_OFFSET, @size*DRAW_TILE, @size*DRAW_TILE, true, finished ? Engine::Color::WHITE : Engine::Color::RED)
    @applications.each do |appl|
      templ = appl.templ
      index = @templates.index(templ).not_nil!
      Engine.rect(DRAW_OFFSET + appl.x0*DRAW_TILE, DRAW_OFFSET + appl.y0*DRAW_TILE, templ.width*DRAW_TILE, templ.height*DRAW_TILE, false, DRAW_COLORS[index % DRAW_COLORS.size])
      TemplateExitType.each do |typ|
        appl.each_exit(typ, false) do |loc|
          debug_draw_exit(DRAW_OFFSET + (loc.x)*DRAW_TILE, DRAW_OFFSET + loc.y*DRAW_TILE, typ)
        end
      end
      font = @font || init_font
      font.draw_text_boxed(appl.level.to_s, DRAW_OFFSET + appl.x0*DRAW_TILE, DRAW_OFFSET + appl.y0*DRAW_TILE, templ.width*DRAW_TILE, templ.height*DRAW_TILE, halign: HAlign::Center)
    end
    @chains.each do |chain|
      Engine.line(DRAW_OFFSET + chain.start_x*DRAW_TILE, DRAW_OFFSET + chain.start_y*DRAW_TILE, DRAW_OFFSET + chain.end_x*DRAW_TILE, DRAW_OFFSET + chain.end_y*DRAW_TILE, Engine::Color::BLACK)
    end
    @primary_dead_ends.each do |dead|
      Engine.circle(DRAW_OFFSET + dead.x*DRAW_TILE + DRAW_TILE/2, DRAW_OFFSET + dead.y*DRAW_TILE + DRAW_TILE/2, DRAW_TILE/2, true, Engine::Color::BLACK)
    end
    @secondary_dead_ends.each do |dead|
      Engine.circle(DRAW_OFFSET + dead.x*DRAW_TILE + DRAW_TILE/2, DRAW_OFFSET + dead.y*DRAW_TILE + DRAW_TILE/2, DRAW_TILE/3, true, Engine::Color::BLACK)
    end
    Engine.process unless finished
  end

  #######################################################################################################
end

class DebugMapgenSystem < ECS::System
  def initialize(@world)
    @mapgen = GlobalMapGenerator.new(100)
    # init_test_templates
    @mapgen.init_templates_from_json
    @mapgen.check_templates_list
    do_mapgen
  end

  def init_test_templates
    add_template(10, 20) do |templ1|
      templ1.exits[ExitDir::Left.to_i] << TemplateExit.new(TemplateExitType::NormalEnter, 2)
      templ1.exits[ExitDir::Right.to_i] << TemplateExit.new(TemplateExitType::NormalExit, 4)
      8.times { |ax| templ1.exits[ExitDir::Top.to_i] << TemplateExit.new(TemplateExitType::FailEnter, ax + 1) }
      templ1.exits[ExitDir::Bottom.to_i] << TemplateExit.new(TemplateExitType::FailExit, 7)
    end
  end

  def add_template(x, y, &)
    template = MapTemplate.new(x, y)
    yield(template)
    @mapgen.templates << template
  end

  def do_mapgen
    tick = Time.monotonic
    n = 0
    loop do
      @mapgen.fill
      break # if @mapgen.is_good
      n += 1
      break if n > 100
    end

    puts (Time.monotonic - tick).total_milliseconds
    puts @mapgen.status
  end

  def execute
    Engine.layer = 10
    @mapgen.debug_draw(true)

    if Engine::Keys[Engine::Key::Return].pressed?
      do_mapgen
    end
    if Engine::Keys[Engine::Key::Escape].pressed?
      @world.new_entity.add(QuitEvent.new)
    end
  end
end

world = ECS::World.new
systems = ECS::Systems.new(world)
  .add(BasicSystems.new(world))
  .add(DebugMapgenSystem.new(world))
Engine[Params::Height] = 1024
systems.init
loop do
  systems.execute
  break if world.component_exists? QuitEvent
end
systems.teardown
