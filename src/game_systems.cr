require "./ecs"
require "./libnonoengine"
require "./resources"
require "./game_utils"
require "./game_config"
require "./modals"
require "./mechanics/basic_game_systems"
require "./mechanics/platforms"
require "./mechanics/moving_player"
require "./mechanics/level_creation"
require "./mechanics/animation"
require "./mechanics/health"
require "./mechanics/monsters"
require "./mechanics/loot"
require "./mechanics/portals"
require "./mechanics/brother"
require "./mechanics/rope"

class GameSystems < ECS::Systems
  def initialize(@world)
    super
    add ProcessGameState
    add ShowBackgroundSystem
    add KeyReactSystem.new(@world, pressed: CONFIG_PRESSED, down: CONFIG_DOWN)
    add ReactPlayerSystem
    add TurnPlayerSystem
    add MovePlayerSystem
    add JumpPlayerSystem
    add ResetJumpPlayerSystem
    remove_singleframe(MovePlayerEvent)
    remove_singleframe(JumpPlayerEvent)
    add SyncPositionWithPhysicsSystem
    add TrackCameraSystem

    add DrawDebugSystem
    remove_singleframe(InvertDebugRender)

    add ProcessMovingFloorSystem
    remove_singleframe(PlayerOnMovingFloorEvent)
    add CalculatePlayerSpeed
    remove_singleframe(RelativeSpeed)

    add LandFloorSystem
    add FromFloorSystem
    remove_singleframe(PlayerOnFloorEvent)

    add AnimatePlayerSystem
    add ApplyPlayerAnimationSystem
    add RenderTurnedPlayerSystem

    add MovingPlatformsSystem

    add ApplyPressureDamage
    add ApplyMonsterDamage
    add ApplySpikesDamage
    add ApplyAllDamage
    add ShowDamageSystem
    add ShowHPBarSystem

    remove_singleframe(PlayerOnSpikesEvent)
    remove_singleframe(PlayerHitByEnemyEvent)
    remove_singleframe(UnderPressure)
    remove_singleframe(Damage)

    add SkeletonAnimationSystem
    add SkeletonTurnOnWaypoint
    add SkeletonAISystem
    remove_singleframe(EnemyOnWaypoint)

    add GhostAnimationSystem
    add GhostAISystem

    add ShowInventory
    add MakeSomethingLootable
    add RemoveLootOnHitSystem
    remove_singleframe(PlayerHitLootEvent)

    add FlowerThrowingSystem
    add ThrowingSystem
    add ReloadingSystem
    remove_singleframe(ShouldThrow)
    add HitMonsters
    remove_singleframe(EnemyHitByFlower)

    add StartTeleportSystem
    add ProcessTeleportationSystem
    add RemoveAfterTeleportSystem
    remove_singleframe(JumpToPortal)

    add ProcessSkullsUse
    remove_singleframe(UseSkullEvent)
    add ProcessStatueUse
    remove_singleframe(UseStatueEvent)
    add ProcessCompass

    add BrotherAISystem
    add ProcessBrotherTouch
    remove_singleframe(BrotherTouch)

    add RopeThrowingSystem
    add ProcessGrappling
    remove_singleframe(HookCatchWall)
    add CancelRopeSystem
    add RenderRopeSystem
    add ProcessRopeForce
    # add ProcessRopeMotion

    # unimplemented stuff
    remove_singleframe(PlayerOnCrashingFloorEvent)

    add ResetLevelOnDeath
  end
end

THE_PLAYERS = [] of ECS::Entity

def get_the_player
  THE_PLAYERS[0]
end

def create_player(world, x, y)
  THE_PLAYERS.clear
  ent = world.new_entity
  THE_PLAYERS << ent
  ent.add(PartOfLevel.new)
  ent.add(RenderTile.new(image: RES::Hero, frame: 0, dy: -10))
  ent.add(RenderPosition.new(x: 0, y: 0))
  ent.add(RenderRotation.new)
  ent.add(RenderColor.new) # for making red
  ent.add(RenderScale.new) # for turning
  ent.add(RenderLayer.new(layer: 3))
  ent.add(ReceiveMoveEvents.new)
  ent.add(PhysicsBodyAddRequest.new(PhysicsMaterial::Player, x: ((x + 0.5) * cfg.tile_size), y: ((y + 0.5) * cfg.tile_size)))
  ent.add(PlayerAnimation.new)
  # ent.add(PhysicsAddBoxShapeRequest.new(-10, -10, 20, 20))
  ent.add(PhysicsPositionComponent.new(typ: LibEngine::PhysicCoordinatesMode::Read, x: 0, y: 0, vx: 0, vy: 0, a: 0, omega: 0))
  ent.add(TrackingCamera.new)
  ent.add(CheckStand.new)
  ent.add(LookDirection.new(false))
  ent.add(Health.new(100.0))
  ent.add(ShowHealth.new)
  ent.add(DamagedTimer.new)
  ent.add(Teleportable.new)
  ent.add(OneSideTeleportable.new)
end
