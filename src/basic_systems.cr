require "./libnonoengine.cr"
require "./resources.cr"
require "./ecs"

class BasicSystems < ECS::Systems
  def initialize(@world)
    super
    add EngineSystem.new(world)
    add ShouldQuitSystem.new(world)
    add CameraSystem.new(world)
    add RenderSystem.new(world)
  end
end

class EngineSystem < ECS::System
  def init
    Engine[Params::Antialias] = 0
    Engine[Params::VSync] = 1
    Engine[Engine::Params::ClearColor] = 0

    Engine[Params::ProgressWidth] = 671
    Engine[Params::ProgressHeight] = 101
    Engine[Params::ProgressX] = 208
    Engine[Params::ProgressY] = 244

    Engine.init "resources"
    GameConfig.init_config
  end

  def execute
    Engine.process
  end
end

# @[ECS::MultipleComponents]
record RenderSprite < ECS::Component, image : Engine::Sprite, dx : Float32 = 0, dy : Float32 = 0
@[ECS::MultipleComponents]
record RenderTile < ECS::Component, image : Engine::TileMap, frame : Int32, dx : Float32 = 0, dy : Float32 = 0 do
  def set_frame(aframe)
    @frame = aframe
  end
end
record RenderPosition < ECS::Component, x : Float32, y : Float32
record RenderRotation < ECS::Component, angle : Float32 = 0
record RenderScale < ECS::Component, kx : Float32 = 1, ky : Float32 = 1
record RenderLayer < ECS::Component, layer : Int32 = 1
record RenderColor < ECS::Component, color : Engine::Color = Engine::Color::WHITE

class RenderSystem < ECS::System
  def filter(world)
    world.of(RenderPosition).any_of([RenderSprite, RenderTile])
  end

  def process(entity)
    pos = entity.getRenderPosition

    if scale = entity.getRenderScale?
      kx, ky = scale.kx, scale.ky
    else
      kx, ky = 1.0, 1.0
    end
    if rotation = entity.getRenderRotation?
      angle = rotation.angle
    else
      angle = 0.0
    end
    if layer = entity.getRenderLayer?
      Engine.layer = layer.layer
    else
      Engine.layer = 1
    end
    if colorize = entity.getRenderColor?
      color = colorize.color
    else
      color = Color::WHITE
    end
    if spr = entity.getRenderSprite?
      spr.image.draw pos.x + spr.dx, pos.y + spr.dy, kx, ky, angle, color
    end
    if tile = entity.getRenderTile?
      tile.image.draw_frame tile.frame, pos.x + tile.dx, pos.y + tile.dy, kx, ky, angle, color
    end
  end
end

class ShouldQuitSystem < ECS::System
  def execute
    @world.new_entity.add(QuitEvent.new) if !Engine::Keys[Key::Quit].up?
  end
end

alias KeysConfig = Hash(Engine::Key, ECS::Component)

class KeyReactSystem < ECS::System
  @ent : ECS::Entity

  def initialize(@world : ECS::World, *, @pressed = KeysConfig.new, @down = KeysConfig.new)
    @ent = @world.new_entity
  end

  def execute
    @pressed.each do |key, ev|
      if Engine::Keys[key].pressed?
        @ent.set(ev)
      end
    end
    @down.each do |key, ev|
      if !Engine::Keys[key].up?
        @ent.set(ev)
      end
    end
  end
end

@[ECS::SingletonComponent]
record CameraPosition < ECS::Component, dx : Float32 = 0, dy : Float32 = 0, kx : Float32 = 1, ky : Float32 = 1, angle : Float32 = 0 do
  def move(dx, dy)
    return CameraPosition.new(@dx + dx, @dy + dy, @kx, @ky, @angle)
  end

  def scale(k)
    return CameraPosition.new(@dx, @dy, @kx*k, @ky*k, @angle)
  end

  def rotate(angle)
    return CameraPosition.new(@dx, @dy, @kx, @ky, @angle + angle)
  end
end

class CameraSystem < ECS::System
  def init
    @world.new_entity.add(CameraPosition.new)
  end

  def execute
    pos = @world.getCameraPosition
    Engine.camera(pos.dx, pos.dy, pos.kx, pos.ky, pos.angle)
  end
end
