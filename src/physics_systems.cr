require "./libnonoengine.cr"
require "./ecs"

def dump_bodies(world, title)
  pool = world.base_pool_for(PhysicsBodyComponent).as(ECS::Pool(PhysicsBodyComponent))
  p "#{title} bodies state:"
  p "  used: #{pool.total_count}"
  p "  corr: #{pool.@corresponding.map(&.to_s)}"
  p "  sparse: #{pool.@sparse}"
  p "  cache: #{pool.@cache_entity} => #{pool.@cache_index}"
end

class PhysicSystems < ECS::Systems
  def initialize(@world)
    super
    add InitMaterialsSystem
    add ProcessCollisionsSystem

    add PhysicsAddBodySystem
    remove_singleframe(PhysicsBodyAddRequest)

    add PhysicsRemoveBodySystem
    remove_singleframe(PhysicsBodyRemoveRequest)

    # add ECS::RemoveAllOf(PhysicsPositionEvent).new(@world)
    add PhysicsProcessPositionsSystem
    # add ECS::RemoveAllOf(PhysicsPositionReadRequest).new(@world)
    remove_singleframe(PhysicsPositionRequest)

    add PhysicsProcessForcesSystem
    remove_singleframe(PhysicsApplyForceRequest)

    add PhysicsProcessControlSystem
    remove_singleframe(PhysicsApplyControlRequest)

    add PhysicsAddCircleShapesSystem
    remove_singleframe(PhysicsAddCircleShapeRequest)
    add PhysicsAddBoxShapesSystem
    remove_singleframe(PhysicsAddBoxShapeRequest)
    add PhysicsAddLineShapesSystem
    remove_singleframe(PhysicsAddLineShapeRequest)
    add PhysicsAddPolyShapesSystem
    remove_singleframe(PhysicsAddPolyShapeRequest)
  end
end

alias CollisionProcessor = Proc(Tuple(ECS::Entity, ECS::Entity, CollisionData(Nil)), Bool)

abstract struct CollisionComponent < ECS::Component
  getter first : ECS::Entity
  getter second : ECS::Entity
  getter collision : Engine::CollisionData(Nil)

  def initialize(@first, @second, @collision)
  end
end

@[ECS::SingletonComponent]
record PhysicsCollisionMaps < ECS::Component,
  events = Hash(Tuple(PhysicsMaterial, PhysicsMaterial), Tuple(CollisionComponent.class, Bool)).new,
  processor = Hash(Tuple(PhysicsMaterial, PhysicsMaterial), CollisionProcessor).new

class PhysicsConfigurator
  @cur_material : PhysicsMaterial?
  getter maps = PhysicsCollisionMaps.new

  def gravity(x, y)
    Engine[Params::GravityX] = (x*1000).to_i
    Engine[Params::GravityY] = (y*1000).to_i
  end

  def damping(f)
    Engine[Params::Damping] = (f*1000).to_i
  end

  def material(mat : PhysicsMaterial, *, density : Float64, friction : Float64, elasticity : Float64, def_radius : Float64 = 0.0, static : Bool = false, kinematic : Bool = false, non_rotating : Bool = false)
    raise "incorrect configuration syntax" if @cur_material
    typ = LibEngine::BodyType::Dynamic
    typ = LibEngine::BodyType::NonRotating if non_rotating
    typ = LibEngine::BodyType::Kinematic if kinematic
    typ = LibEngine::BodyType::Static if static
    LibEngine.material(mat.unsafe_as(LibEngine::Material), density, friction, elasticity, typ, def_radius)
  end

  def material(mat : PhysicsMaterial, *, density : Float64, friction : Float64, elasticity : Float64, def_radius : Float64 = 0.0, static : Bool = false, kinematic : Bool = false, &block)
    raise "incorrect configuration syntax" if @cur_material
    material(mat, density: density, friction: friction, elasticity: elasticity, def_radius: def_radius, static: static, kinematic: kinematic)
    @cur_material = mat
    yield
    @cur_material = nil
  end

  #   enum CollisionType
  #   Pass
  #   Hit
  #   PassDetect
  #   HitDetect
  #   Processable
  # end

  def collide(mat : PhysicsMaterial, mat2 : PhysicsMaterial = @cur_material)
    raise "incorrect configuration syntax" unless mat2
    LibEngine.material_collisions(mat.unsafe_as(LibEngine::Material), mat2.unsafe_as(LibEngine::Material), LibEngine::CollisionType::Hit)
  end

  def collide_event(mat : PhysicsMaterial, mat2 : PhysicsMaterial, event_type, *, first_only = false)
    LibEngine.material_collisions(mat.unsafe_as(LibEngine::Material), mat2.unsafe_as(LibEngine::Material), LibEngine::CollisionType::HitDetect)
    mat, mat2 = mat2, mat if mat.to_i > mat2.to_i
    @maps.events[{mat, mat2}] = {event_type, first_only}
  end

  def pass(mat : PhysicsMaterial, mat2 : PhysicsMaterial = @cur_material)
    raise "incorrect configuration syntax" unless mat2
    LibEngine.material_collisions(mat.unsafe_as(LibEngine::Material), mat2.unsafe_as(LibEngine::Material), LibEngine::CollisionType::Pass)
  end

  def pass_event(mat : PhysicsMaterial, mat2 : PhysicsMaterial, event_type, *, first_only = false)
    LibEngine.material_collisions(mat.unsafe_as(LibEngine::Material), mat2.unsafe_as(LibEngine::Material), LibEngine::CollisionType::PassDetect)
    mat, mat2 = mat2, mat if mat.to_i > mat2.to_i
    @maps.events[{mat, mat2}] = {event_type, first_only}
  end

  def processable(mat : PhysicsMaterial, mat2 : PhysicsMaterial = @cur_material, &processor : CollisionProcessor)
    raise "incorrect configuration syntax" unless mat2
    LibEngine.material_collisions(mat.unsafe_as(LibEngine::Material), mat2.unsafe_as(LibEngine::Material), LibEngine::CollisionType::Processable)
    mat, mat2 = mat2, mat if mat.to_i > mat2.to_i
    @maps.processor[{mat, mat2}] = processor
  end

  def process(&block)
    with self yield
  end
end

macro config_physics(&block)
  def do_config_physics
    p = PhysicsConfigurator.new
    p.process do 
      {{yield}}
    end
    p.maps
  end
end

require "./materials.cr"

class InitMaterialsSystem < ECS::System
  def init
    ent = @world.new_entity.add(do_config_physics)
  end
end

class ProcessCollisionsSystem < ECS::System
  # @crunch_filter : ECS::Filter
  # def initialize(@world)
  #   @crunch_filter = @world.of(PhysicsBodyComponent)
  # end

  def execute
    # dump_bodies(@world, "iter0")
    @world.getPhysicsCollisionMaps.events.each do |(mat1, mat2), (ev, first_only)|
      # sadly get_material_collisions is broken, so...
      # while LibEngine.get_material_collisions(mat1.unsafe_as(LibEngine::Material), mat2.unsafe_as(LibEngine::Material), out b1, out b2, out afirst, out ax, out ay, out nx, out ny, out aenergy, out aimpulsex, out aimpulsey)
      #   p "event #{b1} #{b2}"
      # end

      @world.base_pool_for(PhysicsBodyComponent).each_entity do |ent|
        # dump_bodies(@world, "iter1")
        body = ent.getPhysicsBodyComponent
        if ent.has? PhysicsBodyRemoveRequest
          # p "wrong collision for #{second}: #{ev}"
          next
        end
        # p ent if body.typ.player?
        next unless body.typ == mat1
        loop do
          second = LibEngine.get_collisions(body.id, mat2.unsafe_as(LibEngine::Material), out afirst, out ax, out ay, out nx, out ny, out aenergy, out aimpulsex, out aimpulsey)
          break if second.unsafe_as(Int64) == LibEngine::NO_BODY_ID.unsafe_as(Int64)
          next if first_only && !afirst
          second = ECS::Entity.new(@world, second.unsafe_as(ECS::EntityID))
          unless second.getPhysicsBodyComponent.typ == mat2
            # p "wrong collision for #{second}: #{ev}"
            next
          end
          if second.has? PhysicsBodyRemoveRequest
            # p "wrong collision for #{second}: #{ev}"
            next
          end
          # p "right collision for #{ent} with #{second}: #{ev}" unless ev == PlayerOnFloorEvent
          coll = Engine::CollisionData(Nil).new(data: nil, first: afirst, x: ax, y: ay, nx: nx, ny: ny, energy: aenergy, impulsex: aimpulsex, impulsey: aimpulsey)
          event = ev.new(first: ent, second: second, collision: coll)
          @world.new_entity.add(event)
        end
      end
    end
  end
end

record PhysicsBodyComponent < ECS::Component, typ : PhysicsMaterial, id : LibEngine::Body

@[ECS::SingleFrame]
record PhysicsBodyAddRequest < ECS::Component, typ : PhysicsMaterial, x : Float64, y : Float64

@[ECS::SingleFrame]
record PhysicsBodyRemoveRequest < ECS::Component, also_entity : Bool

class PhysicsAddBodySystem < ECS::System
  def filter(world) : ECS::Filter?
    world.of(PhysicsBodyAddRequest)
  end

  def process(ent)
    oldbody = ent.getPhysicsBodyComponent?
    req = ent.getPhysicsBodyAddRequest
    LibEngine.body_free(oldbody.id) if oldbody
    id = LibEngine.body_create(req.typ.unsafe_as(LibEngine::Material), ent.id.unsafe_as(LibEngine::BodyID))
    # p "setting coords: #{id.to_i}, #{req.@x}, #{req.@y}"
    # dump_bodies(@world, "adding")
    other = 0.0
    LibEngine.body_coords(id, LibEngine::PhysicCoordinatesMode::Write, pointerof(req.@x), pointerof(req.@y), pointerof(other), pointerof(other), pointerof(other), pointerof(other))
    ent.set(PhysicsBodyComponent.new(req.typ, id))
  end
end

class PhysicsRemoveBodySystem < ECS::System
  def filter(world) : ECS::Filter?
    world.all_of([PhysicsBodyRemoveRequest, PhysicsBodyComponent])
  end

  def process(ent)
    # dump_bodies(@world, "before removal")
    body = ent.getPhysicsBodyComponent
    req = ent.getPhysicsBodyRemoveRequest
    LibEngine.body_free(body.id)

    if req.also_entity
      ent.destroy
    else
      ent.remove(PhysicsBodyComponent)
    end
    # p "done: #{ent}"
    # dump_bodies(@world, "after removal")
  end
end

record PhysicsPositionComponent < ECS::Component,
  typ : LibEngine::PhysicCoordinatesMode,
  x : Float64,
  y : Float64,
  vx : Float64,
  vy : Float64,
  a : Float64,
  omega : Float64

# @[ECS::SingleFrame]
# record PhysicsPositionEvent < ECS::Component,
#   x : Float64,
#   y : Float64,
#   vx : Float64,
#   vy : Float64,
#   a : Float64,
#   omega : Float64

# @[ECS::SingleFrame]
# record PhysicsPositionReadRequest < ECS::Component

@[ECS::MultipleComponents]
@[ECS::SingleFrame]
record PhysicsPositionRequest < ECS::Component,
  typ : LibEngine::PhysicCoordinatesMode,
  x : Float64 = 0,
  y : Float64 = 0,
  vx : Float64 = 0,
  vy : Float64 = 0,
  a : Float64 = 0,
  omega : Float64 = 0,
  use_x : Bool = false,
  use_y : Bool = false,
  use_vx : Bool = false,
  use_vy : Bool = false,
  use_a : Bool = false,
  use_omega : Bool = false do
  def initialize(@typ, *, x = nil, y = nil, vx = nil, vy = nil, a = nil, omega = nil)
    if ax = x
      use_x = true
      @x = ax.to_f64
    end
    if ay = y
      use_y = true
      @y = ay.to_f64
    end
    if avx = vx
      use_vx = true
      @vx = avx.to_f64
    end
    if avy = vy
      use_vy = true
      @vy = avy.to_f64
    end
    if aa = a
      use_a = true
      @a = aa.to_f64
    end
    if aomega = omega
      use_omega = true
      @omega = aomega.to_f64
    end
  end
end

class PhysicsProcessPositionsSystem < ECS::System
  def filter(world) : ECS::Filter?
    # PhysicsPositionReadRequest
    world.any_of([PhysicsPositionComponent, PhysicsPositionRequest]).all_of([PhysicsBodyComponent])
  end

  private def exec_coords(id, typ, comp)
    if typ.read?
      ax, ay, avx, avy, aa, aomega = 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
    else
      ax, ay, avx, avy, aa, aomega = comp.x, comp.y, comp.vx, comp.vy, comp.a, comp.omega
    end
    LibEngine.body_coords(id, typ, pointerof(ax), pointerof(ay), pointerof(avx), pointerof(avy), pointerof(aa), pointerof(aomega))
    return ax, ay, avx, avy, aa, aomega
  end

  private def exec_request(id, typ, comp)
    if typ.read?
      ax, ay, avx, avy, aa, aomega = 0.0, 0.0, 0.0, 0.0, 0.0, 0.0
    else
      ax, ay, avx, avy, aa, aomega = comp.x, comp.y, comp.vx, comp.vy, comp.a, comp.omega
    end
    LibEngine.body_coords(id, typ,
      comp.use_x ? pointerof(ax) : Pointer(Float64).null,
      comp.use_y ? pointerof(ay) : Pointer(Float64).null,
      comp.use_vx ? pointerof(avx) : Pointer(Float64).null,
      comp.use_vy ? pointerof(avy) : Pointer(Float64).null,
      comp.use_a ? pointerof(aa) : Pointer(Float64).null,
      comp.use_omega ? pointerof(aomega) : Pointer(Float64).null)
  end

  def process(ent)
    body = ent.getPhysicsBodyComponent
    if comp = ent.getPhysicsPositionComponent?
      physics_tuple = exec_coords(body.id, comp.typ, comp)
      if comp.typ.read? || comp.typ.read_write?
        ent.update(PhysicsPositionComponent.new(comp.typ, *physics_tuple))
      end
    end
    if comp = ent.getPhysicsPositionRequest?
      raise "incorrect request" if comp.typ.read_write? || comp.typ.read?
      exec_coords(body.id, comp.typ, comp)
    end
    # if comp = ent.getPhysicsPositionReadRequest?
    #   LibEngine.body_coords(body.id, LibEngine::PhysicCoordinatesMode::Read, out ax, out ay, out avx, out avy, out aa, out aomega)
    #   ent.add(PhysicsPositionEvent.new(x: ax, y: ay, vx: avx, vy: avy, a: aa, omega: aomega))
    # end
  end
end

@[ECS::MultipleComponents]
@[ECS::SingleFrame]
record PhysicsApplyForceRequest < ECS::Component,
  fx : Float64 = 0,
  fy : Float64 = 0,
  dx : Float64 = 0,
  dy : Float64 = 0,
  torque : Float64 = 0

class PhysicsProcessForcesSystem < ECS::System
  def filter(world) : ECS::Filter?
    world.all_of([PhysicsApplyForceRequest, PhysicsBodyComponent])
  end

  def process(ent)
    req = ent.getPhysicsApplyForceRequest
    body = ent.getPhysicsBodyComponent
    LibEngine.body_apply_force(body.id, req.fx, req.fy, req.dx, req.dy, req.torque)
  end
end

@[ECS::SingleFrame]
record PhysicsApplyControlRequest < ECS::Component,
  tx : Float64,
  ty : Float64,
  max_speed : Float64,
  max_force : Float64

record PhysicsApplyControl < ECS::Component,
  tx : Float64,
  ty : Float64,
  max_speed : Float64,
  max_force : Float64

class PhysicsProcessControlSystem < ECS::System
  def filter(world) : ECS::Filter?
    world.any_of([PhysicsApplyControlRequest, PhysicsApplyControl]).of(PhysicsBodyComponent)
  end

  def process(ent)
    body = ent.getPhysicsBodyComponent
    req = ent.getPhysicsApplyControl?
    LibEngine.body_apply_control(body.id, req.tx, req.ty, req.max_speed, req.max_force) if req
    req = ent.getPhysicsApplyControlRequest?
    LibEngine.body_apply_control(body.id, req.tx, req.ty, req.max_speed, req.max_force) if req
  end
end

@[ECS::SingleFrame]
@[ECS::MultipleComponents]
record PhysicsAddCircleShapeRequest < ECS::Component,
  dx : Float64,
  dy : Float64,
  r : Float64

@[ECS::SingleFrame]
@[ECS::MultipleComponents]
record PhysicsAddBoxShapeRequest < ECS::Component,
  x1 : Float64,
  y1 : Float64,
  x2 : Float64,
  y2 : Float64

@[ECS::SingleFrame]
@[ECS::MultipleComponents]
record PhysicsAddLineShapeRequest < ECS::Component,
  x1 : Float64,
  y1 : Float64,
  x2 : Float64,
  y2 : Float64

@[ECS::SingleFrame]
@[ECS::MultipleComponents]
record PhysicsAddPolyShapeRequest < ECS::Component,
  points : Array(Tuple(Float64, Float64))

class PhysicsAddCircleShapesSystem < ECS::System
  def filter(world) : ECS::Filter?
    world.of(PhysicsAddCircleShapeRequest).of(PhysicsBodyComponent)
  end

  def process(ent)
    body = ent.getPhysicsBodyComponent
    req = ent.getPhysicsAddCircleShapeRequest
    LibEngine.body_add_shape_circle(body.id, req.dx, req.dy, req.r)
  end
end

class PhysicsAddBoxShapesSystem < ECS::System
  def filter(world) : ECS::Filter?
    world.of(PhysicsAddBoxShapeRequest).of(PhysicsBodyComponent)
  end

  def process(ent)
    body = ent.getPhysicsBodyComponent
    req = ent.getPhysicsAddBoxShapeRequest
    LibEngine.body_add_shape_box(body.id, req.x1, req.y1, req.x2, req.y2)
  end
end

class PhysicsAddLineShapesSystem < ECS::System
  def filter(world) : ECS::Filter?
    world.of(PhysicsAddLineShapeRequest).of(PhysicsBodyComponent)
  end

  def process(ent)
    body = ent.getPhysicsBodyComponent
    req = ent.getPhysicsAddLineShapeRequest
    LibEngine.body_add_shape_line(body.id, req.x1, req.y1, req.x2, req.y2)
  end
end

class PhysicsAddPolyShapesSystem < ECS::System
  def filter(world) : ECS::Filter?
    world.of(PhysicsAddPolyShapeRequest).of(PhysicsBodyComponent)
  end

  def process(ent)
    body = ent.getPhysicsBodyComponent
    points = ent.getPhysicsAddPolyShapeRequest.points

    poly = LibEngine.polygon_create(points.size)
    points.each do |(x, y)|
      LibEngine.polygon_add_point(poly, x, y)
    end
    LibEngine.body_add_shape_poly(body.id, poly)
  end
end
