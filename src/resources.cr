include Engine

THE_SCREEN = Sprite.new(-1)
NO_MUSIC = Sound.new(-1)
DEFAULT_SHADER = DefaultShader.new(0)
ALL_SHADERS = ShaderAllShaders.new(-1)
    
module RES
  module Music
    Intro = Sound.new(0)
    Level1 = Sound.new(1)
    Level2 = Sound.new(2)
    Over = Sound.new(3)
  end

  Background = Sprite.new(0)
  Background2 = Sprite.new(1)
  Catrine = Sprite.new(2)
  Catrine_big = Sprite.new(3)
  Compass = Sprite.new(4)
  Compass_back = Sprite.new(5)
  Compass_front = Sprite.new(6)
  Config = RawResource.new(0)
  Flower = Sprite.new(7)
  Font = FontResource.new(0)
  Ghost = TileMap.new(0)
  Graveyard = Sprite.new(8)
  Ground = TileMap.new(1)
  Hero = TileMap.new(2)
  Hp_back = Sprite.new(9)
  Hp_empty = Sprite.new(10)
  Hp_full = Sprite.new(11)
  Intro1 = Sprite.new(12)
  Loading_back = Sprite.new(13)
  Loading_bar = Sprite.new(14)
  Press_enter = Sprite.new(15)
  Rope = Sprite.new(16)
  Script = FontResource.new(1)
  Scroll = Sprite.new(17)
  Skeleton = TileMap.new(3)
  Skull = Sprite.new(18)
  Snow = TileMap.new(4)
  Star = TileMap.new(5)
  Surface = TileMap.new(6)
end


class ShaderAllShaders < Shader
  uniform screen_size, vec2, 0
  uniform tex, sampler2D, 1
  attribute color, vec4, 0
  attribute pos, vec3, 1
  attribute texpos, vec3, 2
end
