enum TutorialMessages
  Welcome1        # +
  Welcome2        # +
  FoundFlower     # +
  FoundSkull      # +
  FoundStatue     # +
  FoundCompass    # +
  FoundRope       # +
  UsedSkull       # +
  UsedStatue      # +
  HitByMonster    # +
  KilledMonster   # +
  GotoFirstLevel  # +
  GotoSecondLevel # +
  GotoThirdLevel  # +
  WaitForBrother  # +
  GotoFourthLevel # +
  Won1            # +
  Won2            # +
  Won3            # +
  # --------------
  AlwaysShown
  # --------------
  RescuedByStatue  # +
  KilledByPressure # +
  KilledBySpikes   # +
  KilledByMonster  # +
end

MSG_TO_LOOT = {
  TutorialMessages::FoundFlower  => LootType::Flower,
  TutorialMessages::FoundSkull   => LootType::Skull,
  TutorialMessages::FoundStatue  => LootType::Statue,
  TutorialMessages::FoundCompass => LootType::Compass,
  TutorialMessages::FoundRope    => LootType::Rope,
  TutorialMessages::UsedSkull    => LootType::Skull,
  TutorialMessages::UsedStatue   => LootType::Statue,
}

require "./literacy.cr"

MAX_WIDTH = 40

ITEM_X    = 500
ITEM_Y    = 400
ITEM_SIZE = 2.0

TUTORIAL_SHOWN = Set(TutorialMessages).new

def test_modals
  TutorialMessages.each do |msg|
    show_modal(msg) unless msg == TutorialMessages::AlwaysShown
  end
end

def show_modal(msg)
  return if msg < TutorialMessages::AlwaysShown && TUTORIAL_SHOWN.includes?(msg)
  TUTORIAL_SHOWN << msg if msg < TutorialMessages::AlwaysShown
  text = MODAL_TEXTS[msg]
  Engine[Engine::Params::PhysicsSpeed] = 0

  loop do
    if msg.welcome1?
      RES::Intro1.background
    else
      RES::Scroll.background
    end
    if msg.won3? || msg > TutorialMessages::RescuedByStatue
      RES::Music::Over.music
    else
      RES::Press_enter.draw(Engine[Engine::Params::Width] / 2, Engine[Engine::Params::Height] / 2)
    end

    if item = MSG_TO_LOOT[msg]?
      LOOT_IMG[item.to_i].draw(ITEM_X, ITEM_Y, kx: ITEM_SIZE, ky: ITEM_SIZE)
    end

    if msg.goto_first_level?
      RES::Catrine_big.draw(850, 450)
    end
    if msg.goto_third_level?
      RES::Hero.draw_frame(1, 450, 540)
      RES::Hero.draw_frame(5, 500, 500, kx: -1.5, ky: 1.5, color: Engine.color(255, 255, 255, 150))
    end

    font = Fonts[F::Modals]
    panel(150, 100, Engine[Engine::Params::Width], Engine[Engine::Params::Height], HAlign::Center, VAlign::Center) do
      text.split('|').each do |s|
        if s.size > MAX_WIDTH
          list = [] of String
          line = ""
          s.split(' ').each do |tok|
            if tok.size + line.size < MAX_WIDTH
              line = (line == "") ? tok : (line + " " + tok)
            else
              list << line
              line = tok
            end
          end
          list << line unless line == ""
          list.each do |line|
            label(line, font, 10, 5, 300, 35, HAlign::Left, VAlign::Flow)
          end
        else
          label(s, font, 10, 5, 300, 35, HAlign::Left, VAlign::Flow)
        end
      end
    end

    # font.draw_text_boxed(text, 150, 100)

    Engine.process
    break if Engine::Keys[Engine::Key::Quit].pressed?
    unless msg.won3? || msg > TutorialMessages::RescuedByStatue
      break if Engine::Keys[Engine::Key::Return].pressed?
    end
  end

  Engine[Engine::Params::PhysicsSpeed] = 1000
end
