require "json"
require "./basic_systems"

CONFIG_PRESSED = KeysConfig.new
CONFIG_PRESSED[Engine::Key::Escape] = QuitEvent.new
CONFIG_PRESSED[Engine::Key::P] = InvertDebugRender.new

CONFIG_PRESSED[Engine::Key::P] = InvertDebugRender.new
CONFIG_PRESSED[Engine::Key::P] = InvertDebugRender.new

CONFIG_PRESSED[Engine::Key::F1] = UseStatueEvent.new
CONFIG_PRESSED[Engine::Key::F2] = UseSkullEvent.new

CONFIG_DOWN = KeysConfig.new

CONFIG_DOWN[Engine::Key::A] = MovePlayerEvent.new(dx: -1)
CONFIG_DOWN[Engine::Key::D] = MovePlayerEvent.new(dx: 1)
CONFIG_DOWN[Engine::Key::W] = JumpPlayerEvent.new

CONFIG_DOWN[Engine::Key::Left] = MovePlayerEvent.new(dx: -1)
CONFIG_DOWN[Engine::Key::Right] = MovePlayerEvent.new(dx: 1)
CONFIG_DOWN[Engine::Key::Up] = JumpPlayerEvent.new

enum F
  Debug
  Inventory
  Level
  Modals
end

Fonts = {} of F => Engine::Font

def init_fonts
  Fonts[F::Debug] = Font.new(RES::Font, char_size: 12, color: Color::BLACK)
  Fonts[F::Inventory] = Font.new(RES::Font, char_size: 24, color: Color::BLACK)
  Fonts[F::Level] = Font.new(RES::Font, char_size: 36, color: Color::BLACK)
  Fonts[F::Modals] = Font.new(RES::Script, char_size: 36, color: Color::BLACK)
end

class GameConfig
  include JSON::Serializable

  getter player_walk_force : Float64
  getter player_air_walk_force : Float64
  getter player_stop_force : Float64
  getter player_jump_force : Float64
  getter player_jump_force_acc : Float64
  getter player_max_jump_force : Float64
  getter platform_speed_x : Float64
  getter platform_speed_y : Float64
  getter player_speed : Float64
  getter tile_size : Int32
  getter gravity : Float64
  getter air_friction : Float64
  getter floor_friction : Float64
  getter floor_elastic : Float64
  getter player_friction : Float64
  getter player_elastic : Float64
  getter player_weight : Float64
  getter loot_weight : Float64
  getter enemy_weight : Float64
  getter enemy_force : Float64
  getter enemy_speed : Float64
  getter pressure_damage : Float64
  getter monster_damage : Float64
  getter monster_hit_force : Float64
  getter damage_show_color_s : String
  getter damage_show_color : UInt32 = 0

  getter damage_show_time : Int32
  getter throw_flower_speed : Float64
  getter throw_flower_delay : Int32

  getter teleport_speed : Float64
  getter after_teleport_delay : Int32

  getter camera : Float64
  getter spikes_jump : Float64
  getter spikes_damage : Float64

  getter ghost_speed : Float64
  getter ghost_range : Float64

  getter skeleton_chance : Float64
  getter ghost_chance : Float64

  getter brother_range : Float64

  getter rope_piece : Int32
  getter throw_rope_speed : Float64
  getter rope_base_force : Float64
  # getter rope_max_speed : Float64
  # getter rope_up_force : Float64
  # getter rope_down_force : Float64
  getter speedcontrol_scale : Float64
  getter speedcontrol_zero : Float64

  @@instance : GameConfig?

  def self.instance
    @@instance.not_nil!
  end

  def self.init_config
    @@instance = GameConfig.new(JSON::PullParser.new(RES::Config.as_string))
    @@instance.not_nil!.patch
    init_fonts
  end

  def patch
    @damage_show_color = UInt32.new(@damage_show_color_s, prefix: true)
  end
end

def cfg
  GameConfig.instance
end
